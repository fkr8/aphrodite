// $('.pic_img').on('click', function () {
//   let src = $(this).attr('src');
//   $('#product_image').attr('src', src);
// });

$(function () {

  // 拡大用
  $('.big').slick({
    asNavFor: '.slider',
    waitForAnimate: false,
    // appendArrows: $('#arrows'), //左右矢印の変更でうまく動作しない
  });

  // 本体用
  $('.slider').slick({
    asNavFor: '.big',
    dots: true,
    waitForAnimate: false,
  });

  // サムネイル用
  $('.thumb').slick({
    asNavFor: '.slider,.big',
    focusOnSelect: true,
    slidesToShow: 5,
    vertical: true,
    waitForAnimate: false,
  });

  // 画像スライドとクリックの区別 (静的クリック)
  let isSelect = false;
  $('.slider img').on('mousedown', function () {
    isSelect = true;
  });
  $('.slider img').on('mousemove', function () {
    isSelect = false;
  });
  $('.slider img').on('mouseup', function () {
    if (isSelect) {
      $('.disp').css('display', 'block');
      $('.big').slick('setPosition');
    }
  });

  // 画像スライドとキャンセルの区別
  let isClear = false;
  $('.disp').on('mousedown', function (event) {
    isClear = true;
  });
  $('.disp').on('mouseup', function (event) {
    if (isClear) {
      $('.disp').css('display', 'none');
    }
  });
  $('.big').on('mousedown mouseup', function (event) {
    event.stopPropagation();
    event.preventDefault();
    isClear = false;
  });

  //ドロップダウンリストからdetailの値を取得
  $('[name=color,name=size]').change(function() {
    // 選択されているvalue属性値を取り出す
    var val = $('[name=color,name=size]').val();
    console.log(val); // 出力：
  });
});
