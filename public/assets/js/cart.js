// 総計計算
let allCalc = function (kind) {
  let allArray = $(`.sum-${kind}`).map(function () {
    return $(this).data(`sum-${kind}`);
  }).toArray();
  let all = allArray.reduce((a, x) => a += x, 0);
  return all;
};

// 初手総計表示
$(function () {
  $(".all-price").text(`合計：￥${(allCalc("price")).toLocaleString()}`);
  // $(".all-unit").text(`￥${(allCalc("unit")).toLocaleString()}`);
  // $(".all-tax").text(`￥${(allCalc("tax")).toLocaleString()}`);
});

//数量変更
$(".quantity").change(function () {
  // 数量
  let quantity = $(this).val();
  let detailId = $(this).data("detail_id");
  let obj = { "quantity": quantity, "detailId": detailId };
  let url = '/cart_quantity';
  ajax(url, obj);
  changeView(this, quantity);
});

// 削除
$(".del-btn").on('click', function () {
  if (confirm('カートから削除します。よろしいですか？')) {
    let detailId = $(this).data("detail_id");
    let obj = { "detailId": detailId };
    let url = "/cart_delete"
    ajax(url, obj)
    changeView(this, 0);
    // 非表示
    $(this).parent().parent().css("display", "none");
  }
});

//Ajax
const ajax = function (url, obj) {
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    url: url,
    type: 'post',
    data: obj,
    dataType: "json",
    async: true,
    timeout: 5000,
    success: function () {
      //通信が成功した場合の処理
      console.log("通信成功");
    },
    error: function () {
      //通信が失敗した場合の処理
      console.log("リクエスト時になんらかのエラーが発生しました：");
    }
  });
};


// 表示変更
let changeView = function (tag, quantity) {
  // 元値
  let price = $(tag).parent().siblings(".price").data("price");
  // 小計タグ
  let sumPriceTag = $(tag).parent().siblings(".sum-price");
  // 小計data
  sumPriceTag.data("sum-price", quantity * price);
  // 小計表示
  sumPriceTag.text(`￥${(quantity * price).toLocaleString()}`);
  // 総計表示
  $(".all-price").text(`合計：￥${(allCalc("price")).toLocaleString()}`);
}

// // 表示変更
// let changeView = function (tag, quantity) {
//   // 元値
//   let unit = $(tag).siblings(".unit").data("unit");
//   let tax = $(tag).siblings(".tax").data("tax");
//   let price = $(tag).siblings(".price").data("price");
//   // 小計タグ
//   let sumUnitTag = $(tag).siblings(".sum-unit");
//   let sumTaxTag = $(tag).siblings(".sum-tax");
//   let sumPriceTag = $(tag).siblings(".sum-price");
//   // 小計data
//   sumUnitTag.data("sum-unit", quantity * unit);
//   sumTaxTag.data("sum-tax", quantity * tax);
//   sumPriceTag.data("sum-price", quantity * price);
//   // 小計表示
//   sumUnitTag.text(`￥${(quantity * unit).toLocaleString()}`);
//   sumTaxTag.text(`￥${(quantity * tax).toLocaleString()}`);
//   sumPriceTag.text(`￥${(quantity * price).toLocaleString()}`);
//   // 総計表示
//   $(".all-price").text(`￥${(allCalc("price")).toLocaleString()}`);
//   $(".all-unit").text(`￥${(allCalc("unit")).toLocaleString()}`);
//   $(".all-tax").text(`￥${(allCalc("tax")).toLocaleString()}`);
// }
