/*
ドラッグ&ドロップと通常の選択でファイルのセット方法が異なるため、処理がややこしくなってる

ドラッグ&ドロップ：dataBoxを仲介役の箱として使用し#pic_fileにセット
通常の選択：選択時点で#pic_fileにセットされる ⇒ dataBoxに追加して#pic_fileに再セット

バリデーションエラーの場合
dataBoxにセット時に新規画像(重複していない画像)をnewFiles[]に保管
エラーの場合は新規画像分をdataBoxから削除

画像削除時
登録済みの画像を削除するのと、追加した画像を削除するのでも処理が異なる
⇒ サーバーへのファイル保存時に画像の名前が変化するため、差別化が必要
*/

//仲介用の箱
let dataBox = new DataTransfer();

// 要素ドロップ時
$('#pic_area').on('drop', function (event) {
  event.preventDefault();
  let files = event.originalEvent.dataTransfer.files;
  setFiles(files);
});

// 通常の選択
$('#pic_file').on('change', function () {
  setFiles(this.files);
});

//fileセット
function setFiles(files) {
  files = deleteSame(files);
  let isVali = vali(dataBox.files);
  if (isVali) {
    showFiles(files);
  } else {
    $.each(files, function (i, file) {
      dataBox.items.remove(dataBox.items.length - 1);
    });
  }
  $('#pic_file').prop('files', dataBox.files);
}

//重複が無ければdataBoxにfileセット
function deleteSame(files) {
  var newFiles = [];
  $.each(files, function (i, newFile) {
    var isSame = true;
    $.each(dataBox.files, function (j, oldFile) {
      if (newFile.name == oldFile.name) {
        isSame = false;
        return false;
      }
    });
    if (isSame) {
      dataBox.items.add(newFile);
      newFiles.push(newFile);
    }
  });
  return newFiles;
}

//画像バリデーション
function vali(files) {
  // 選択枚数判定
  let initLength = $('.pic_img_ini').length;
  if (initLength + files.length > 5) {
    alert('アップロードできる画像は5つまでです');
    return false;
  }
  // ファイル無し
  if (files.length == 0) {
    $('#cancel').click();
    return false;
  }
  // 画像ファイル判定(無くてもいい？)
  for (let i = 0; i < files.length; i++) {
    if (!files[i].type.match('image.*')) {
      alert('画像を選択してください');
      return false;
    }
  }
  return true;
}

//画像表示
function showFiles(files) {
  $('#no_image').css('display', 'none')
  $.each(files, function (i, file) {
    var reader = new FileReader();
    reader.onload = function (event) {
      var src = event.target.result;
      var imgContent =
        `<div class="pic_box" style="width: auto; height: 100%;">
          <input class="cancel" type="button" value="削除" style="position: absolute;">
          <img class="pic_img" src=${src} alt=${file.name} style="width: 100px; height: auto;">
        </div>`;
      $("#pic_area").append(imgContent);
    }
    reader.readAsDataURL(files[i]);
  });
}

// cancelボタン
$(document).on('click', '.cancel', function () {
  let picBox = $(this).parent();
  let picImg = $(this).next();
  $.each(dataBox.files, function (i, file) {
    if (file.name == picImg[0].alt) {
      dataBox.items.remove(i);
    }
  });
  $('#pic_file').prop('files', dataBox.files);
  picBox.remove();
  let initLength = $('.pic_img_ini').length;
  if (initLength + dataBox.files.length == 0) {
    $('#no_image').css('display', 'block');
  }
});

// ドラッグしている要素がドロップ領域に入ったとき・領域にある間
$('#pic_area').on('dragenter dragover', function (event) {
  event.stopPropagation();
  event.preventDefault();
});

// 対象以外でのドロップ防止
$(document).on('dragenter dragover drop', function (event) {
  event.stopPropagation();
  event.preventDefault();
});

//iniキャンセル
$('.cancel_ini').on('click', function () {
  $(this).parent().remove();
  let initLength = $('.pic_img_ini').length;
  if (initLength + dataBox.files.length == 0) {
    $('#no_image').css('display', 'block');
  }
});

//statusチェック
$('#status').on('click', function () {
  status();
});
//ロード時statusチェック
$(function () {
  status();
});
//チェック関数
let status = function () {
  if ($('#status').prop('checked')) {
    $('#release_data').css('display', 'block');
  } else {
    $('#release_data').css('display', 'none');
  }
}
