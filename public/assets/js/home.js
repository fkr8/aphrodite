// $('.pic_img').on('click', function () {
//   let src = $(this).attr('src');
//   $('#product_image').attr('src', src);
// });


//「月別データ」
var mydata = {
    labels: Laravel.date,
    datasets: [{
        label: '円',
        hoverBackgroundColor: "rgba(255,99,132,0.3)",
        data: Laravel.sales,
    }]
};

//「オプション設定」
var options = {
    title: {
        display: false,
        text: 'サンプルチャート'
    }
};

var canvas = document.getElementById('stage');
var chart = new Chart(canvas, {

    type: 'bar', //グラフの種類
    data: mydata, //表示するデータ
    options: options //オプション設定
});

