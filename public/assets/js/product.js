//新規登録
$("#register").on('click', function () {
  location.href = '/product_regist';
});

//編集
function Edit(id) {
  location.href = `/product_edit/${id}`
}

//削除
$('#productDel').on('click', function () {
  if (confirm('選択された行を削除します。よろしいですか？')) {
    $('#form').submit();
  }
});
