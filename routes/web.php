<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('welcome');
});

//会員
Route::get('/', 'UsersController@index');

Route::post('user_search', 'UsersController@userPost');//会員管理

Route::post('user_delete', 'UsersController@userDelete');


Route::post('user_edit','UsersController@userEdit');//編集画面の出力
Route::get('user_edit','UsersController@userEdit');

Route::post('user_update','UsersController@update');//編集内容の登録

//コンテンツ
Route::get('content', 'ContentController@index');//コンテンツ管理
Route::get('content_resist', function () {//コンテンツ新規登録
  return view('content/contentResist');
});
Route::post('resist_success', 'ContentController@create');

Route::post('content_delete', 'ContentController@contentDelete');//削除

Route::post('content_edit','ContentController@contentEdit');//編集画面の出力
Route::get('content_edit','ContentController@contentEdit');

Route::post('content_update','ContentController@update');

// 受注
Route::get('order', 'OrdersController@index'); //受注管理
Route::post('order_delete', 'OrdersController@orderDelete');

//プロダクト
Route::get('product', 'ProductsController@index'); //管理
Route::post('product_search', 'ProductsController@productPost'); //検索
Route::get('product_edit/{id}', 'ProductsController@productEdit'); //編集
Route::post('product_update', 'ProductsController@productUpdate'); //編集登録
Route::post('product_delete', 'ProductsController@productDelete'); //削除
Route::get('product_regist', function(){ //新規登録
  return view('products/productRegist');
});
Route::post('product_regist', 'ProductsController@productRegist'); //新規登録
Route::get('product_detail/{id}', 'UserHomeController@indexDetail'); //商品詳細

//カート
Route::get('cart', 'CartController@cartConfirm'); //カート確認
// Route::get('cart', 'CartController@cartDetails'); //カート確認（product_detail_id)
Route::post('cart', 'CartController@cartAdd'); //カートに商品追加
Route::post('cart_delete', 'CartController@cartDelete'); //カート内商品削除
Route::post('cart_quantity', 'CartController@cartChangeQuantity'); //カート内数量変更
Route::get('purchase', 'CartController@cartPurchase'); //カート内購入確認
Route::get('purchase_complete', 'CartController@purchaseComplete'); //カート内購入処理
Route::get('complete', function(){ //完了画面
  return view('users/complete');
});

//コンタクト
Route::get('contact', function(){ //お問い合わせ
  return view('contact/contact');
});
Route::post('contact_comfirm', 'ContactController@contactComfirm'); //お問い合わせ確認
Route::post('contact_send', 'ContactController@contactSend'); //お問い合わせ送信

//ログイン
Route::get('login','UsersLoginController@getLogin');
//ログインからのPOST処理
Route::post('login','UsersLoginController@postLogin');

//ログアウト画面(仮)
// Route::get('logout', function(){
//   return redirect('/user_home');
// });
//ログアウト処理
Route::get('logout','UsersLogoutController@postLogout');

//会員登録画面
Route::get('regist','RegistsController@getRegist');
Route::post('regist','RegistsController@postRegist');

//NEWS
Route::get('user_news', 'UsersNewsController@getNews');

// HOME(管理側)
Route::get('home', 'HomeController@index'); //HOME

// HOME(ユーザ)
Route::get('user_home', 'UserHomeController@index'); //HOME
