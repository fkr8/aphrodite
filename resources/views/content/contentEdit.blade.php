@extends('layouts.adminapp')
@section('title', 'content Edit')
@section('content')
<h1 class="text-center">コンテンツ編集</h1>
<form id="form" method="POST" action="content_update" class="form-group">
    @csrf
    内容<br>
    <input type="hidden" name="about_id" value="{{$items->about_id}}">    
    
    <textarea name="about" rows="10" cols="80">{{$items->about}}</textarea>
    
    <!-- @if($errors->has('about'))<br><span class="error">{{ $errors->first('about') }}</span> @endif -->

    <br>
    <input type="hidden" name="update_at" value="{{date('Y-m-d H:i:s')}}">
    {{ csrf_field() }}
    <input type="submit" onclick="return Edit();" value="登録">
</form>

<script type="text/javascript" src="{{ asset('assets/js/contentEdit.js') }}"> </script>

@endsection