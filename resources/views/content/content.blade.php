@extends('layouts.adminapp')
@section('title', 'content management')
@section('content')
<h1 class="text-center">コンテンツ管理</h1>
<div class="flex">
    <form method="POST" action="content_resist">
        <!--新規登録-->
        @csrf
        <input type="button" onclick="location.href='content_resist'" name="content_resist" value="新規登録">
    </form>

    <form action="content_edit" method="post" id="formEdit">
        <!--編集hidden-->
        {{ csrf_field() }}
        <input type="hidden" id="aboutId" name="about">
    </form>

    <form id="form" method="POST" action="content_delete" class="contentDel">
        <!--削除-->
        @csrf
        <input type="submit" id="content_del" onclick="return Delete();" value="削除">
</div>


<table class="table table-hover table-fixed">
    <thead>
        <tr>
            <th>✔</th>
            <th>ID </th>
            <th>ABOUT</th>
            <th>作成日時</th>
            <th>更新日時</th>
        </tr>
    </thead>
    <tbody>
        @foreach($items as $item)
        <tr>
            <td class="text-center"><input type="checkbox" class="checkItem" id="checkItem" name="check[{{$loop->index}}]" value="{{$item->about_id}}"></td>
            <td name="about_id" onclick="Edit('{{$item->about_id}}')" class="td01">{{$item->about_id}}</td>
            <td name="about" onclick="Edit('{{$item->about_id}}')" class="td02">{{$item->about}}</td>
            <td name="created_at" onclick="Edit('{{$item->about_id}}')" class="td03">{{$item->created_at}}</td>
            <td name="update_at" onclick="Edit('{{$item->about_id}}')" class="td04">{{$item->update_at}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</form>
{{ $items->links() }}

<script>
    //削除
    function Delete() {
        if (window.confirm('選択された行を削除します。よろしいですか？')) { // 確認ダイアログを表示
            $('#form').prop('action', 'content_delete');
            $('#form').submit(); // 確認ダイアログを表示
            return true; // 「OK」時は送信を実行
        } else { // 「キャンセル」時の処理
            //window.alert('キャンセルされました'); // 警告ダイアログを表示
            return false; // 送信を中止
        }
    }

    //編集
    function Edit(id) {
        if (id != null) {
            $("#aboutId").val(id);
            $('#formEdit').submit();
        }
    }
</script>
@endsection
