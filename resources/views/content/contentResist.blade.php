@extends('layouts.adminapp')
@section('title', 'contentResist')
@section('content')
<h1 class="text-center">コンテンツ登録</h1>
<form id="form" action="resist_success" method="POST">
    @csrf
    内容<br>
    <textarea type="text" name="about" rows="10" cols="80"></textarea>
    <br>
    <input type="hidden" name="created_at" value="{{date('Y-m-d H:i:s')}}">
    {{ csrf_field() }}
    <input type="submit" onclick="return Resist();" value="登録">
</form>
<script type="text/javascript" src="{{ asset('assets/js/contentResist.js') }}"> </script>
@endsection