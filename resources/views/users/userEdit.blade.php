@extends('layouts.adminapp')
@section('title', 'user edit')
@section('content')
<h1 class="text-center">会員編集</h1>
<tbody>

  <div class="container">
    <form method="POST" action="user_update" class="form-group">
      <!--会員編集フォーム-->
      @csrf
      <label>氏名</label><br>
      <input type="hidden" name='user_id' value="{{$items->user_id}}">
      <!-- @error('edit_name')
      <p>ERROR:{{$message}}</p>
      @enderror -->
      <input type="name" class="form-control" name='name' value="{{$items->name}}">
      <br><label>電話番号</label><br>
      <input type="tel" class="form-control" name='tel' value="{{$items->tel}}">
      <br><label>メールアドレス</label><br>
      <input type="email" class="form-control" name='email' value="{{$items->email}}">
      <br><label>年齢</label><br>
      <input type="age" class="form-control" name='age' value="{{$items->age}}"><br>
      <input type="submit" value="登録">
  </div>
  </form>
</tbody>
@endsection