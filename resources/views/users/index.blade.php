@extends('layouts.adminapp')
@section('title', 'user management')
@section('content')
<h1 class="text-center">会員管理</h1>
<div class="flex">
    <form method="POST" action="user_search">
        <!--会員検索フォーム-->
        @csrf
        <input type="text" name="user_search" value="{{$user_search = ''}}" placeholder="名前">
        <input type="submit" value="検索">
    </form>

    <form action="user_edit" method="post" id="formEdit">
        <!--編集hidden-->
        {{ csrf_field() }}
        <input type="hidden" id="userId" name="code">
    </form>

    <form id="form" name="form" method="POST" action="user_delete" class="userDel">
        <!--削除-->
        @csrf
        <input type="submit" id="userDel" onclick="return Delete();" value="削除">
</div>
<table class="table table-hover table-fixed">
    <thead>
        <tr>
            <th>✔ </th>
            <th>会員ID </th>
            <th>名 前 </th>
            <th>電話番号 </th>
            <th>メールアドレス</th>
        </tr>
    </thead>
    <tbody>
        @foreach($items as $item)
        <!-- <tr class="check" if($data->del_flg==1) style="background-color:#c3c3c3;" endif> -->
        <tr>
            <td class="text-center"><input type="checkbox" class="checkItem" id="checkItem" name="check[{{$loop->index}}]" value="{{$item->user_id}}"></td>
            <td name="userid" onclick="Edit('{{$item->user_id}}')" class="td01">{{$item->user_id}}</td>
            <td name="name" onclick="Edit('{{$item->user_id}}')" class="td02">{{$item->name}}</td>
            <td name="tel" onclick="Edit('{{$item->user_id}}')" class="td03">{{$item->tel}}</td>
            <td name="email" onclick="Edit('{{$item->user_id}}')" class="td04">{{$item->email}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</form>
{{ $items ?? ''->links() }}

<script>
    //削除
    function Delete() {
        if (window.confirm('選択された行を削除します。よろしいですか？')) { // 確認ダイアログを表示
            $('#form').prop('action', '/userDelete');
            $('#form').submit(); // 確認ダイアログを表示
            return true; // 「OK」時は送信を実行
        } else { // 「キャンセル」時の処理
            //window.alert('キャンセルされました'); // 警告ダイアログを表示
            return false; // 送信を中止
        }
    }

    //編集
    function Edit(id) {
        if (id != null) {
            $("#userId").val(id);
            $('#formEdit').submit();
        }
    }
</script>
@endsection