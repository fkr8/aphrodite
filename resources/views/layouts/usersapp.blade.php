<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title')</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <style>

    body {
      background-color: white;
    }

    tr.isActive {
      background-color: #66FFCC;
    }

    .flex {
      display: inline-flex;
    }

    .userDel {
      padding: 0 0 0 600px
    }

    .font-small {
      font-size: 1rem;
    }

    .users-header,
    .users-footer {
      text-align: center;
    }

    .users-header h1 {
      padding: 20px;
    }

    .users-header a {
      color: #3CB371;
      margin: 20px;
      font-size: 20px;
    }

    .common-menu{
      background-color: #060606;
      margin: 20px;
    }

    .common-btn {
      position: absolute;
      top: 0%;
      right: 0%;
    }

    .common-btn a {
      color: black;
      font-size: 15px;
    }

    .btn{
      background-color: black;
      color: #3CB371;
    }

    .users-content {
      margin-left: 20%;
      width: 60%;
    }
/* ABOUT */
    pre {
	    white-space: pre-wrap ;
    }

  </style>
  <!-- jQuery読み込み -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  @yield('head')
</head>

<body>
  <header class="users-header">
    <h1> <img src="{{ asset('/assets/images/header.jpg') }}"  alt="APHRODITE GANG HOLDINGS" width="600" height="300"/></h1>
    <div class="common-menu">
      <a href="/user_home">HOME</a>
      <a href="/user_news">NEWS</a>
      <a href="/contact">CONTACT</a>
    </div>
    <div class="common-btn">
      @if(session()->has('ses_data'))
        <a href="{{url('/logout')}}">SIGN OUT</a>
      @else
        <a href="{{url('/login')}}">SIGN IN</a>
      @endif
      <a href="{{url('/cart')}}"><img src="{{ asset('/assets/images/cart.png') }}"  alt="カート" width="40" height="40"/></a>
    </div>
  </header>
  <div class="users-content">@yield('content')</div>
  <div class="userHome-content">@yield('userHome_content')</div>
  <footer class="users-footer">
    <div>
      created by <a href="https://fkr8-biz.com/" target="_blank" rel="noopener noreferrer">fkr8</a>
    </div>
  </footer>
</body>

</html>
