<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <style>
        tr.isActive {
            background-color: #66FFCC;
        }

        .flex {
            display: inline-flex;
        }

        .userDel {
            padding: 0 0 0 600px;
        }

        .admin-common {
            position: fixed;
            top: 100px;
        }

        .admin-common a {
            display: block;
            margin: 20px;
            padding: 10px;
            background-color: #E0FFFF;
        }

        .admin-content {
            margin: 0 0 0 200px;
        }

        [testarea] {
            white-space: pre-wrap ;
        }
    
        
    </style>
    <!-- jQuery読み込み -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript">
    $(function () {
        $('.select').on("click", ".add", function() {
            var target = $(this).closest('section');
            target.clone(true).insertAfter(target);
        });
        $('.select').on("click", ".del", function() {
            var target = $(this).closest('section');
            if (target.siblings().length > 1) {
            target.remove(false);
            }
        });
    });
    </script>
    @yield('head')
</head>

<body>
    <aside class="admin-common">
        <a href="/home">HOME</a>
        <a href="/product">商品管理</a>
        <a href="/order">受注管理</a>
        <a href="/">会員管理</a>
        <a href="/content">コンテンツ管理</a>
    </aside>
    <!-- <header>@yield('header')</header>
    <h1 class="text-center">@yield('h1')</h1> -->
    <div class="admin-content">@yield('content')</div>
</body>

</html>