@extends('layouts.adminapp')
@section('title', 'product Edit')
@section('content')
  <h1 class="text-center">商品編集</h1>
  <div class="container">
    <form method="POST" action="/product_update" class="form-group" enctype="multipart/form-data"><!--商品編集フォーム-->
      @csrf
      <input type="hidden" name="product_id" value="{{$items->product_id}}">
      <label>商品名</label>
      @error('product_name')
        <span class="text-danger font-weight-bold ml-5">{{$message}}</span><br>
      @enderror
      <input type="text" class="form-control" name='product_name' value="{{old('product_name') ?? $items->product_name}}"><br>
      <label>商品画像</label>
      <div class="mb-3">
        <div id="pic_area" style="width: 100%; height: 200px; display: flex; justify-content: center; border: 1px solid #ced4da; position: relative;">
          <div class="pic_box_ini" style="width: auto; height: 100%;">
            <img id="no_image" src="{{ asset('/assets/images/noimage.png') }}" alt="No Image" style="width: 100px; height: auto;
              @if(!empty($pics))
                display: none;
              @endif
            ">
          </div>
          @foreach($pics as $pic)
            <div class="pic_box_ini" style="width: auto; height: 100%;">
              <input class="cancel_ini" type="button" value="削除" style="position: absolute;">
              <img class="pic_img_ini" src="/storage/{{$pic}}" alt="product Image" style="width: 100px; height: auto;">
              <input type="hidden" name="pic_remine[]" value="{{$pic}}">
            </div>
          @endforeach
        </div>
        枠内に画像をドラッグ&ドロップ　または　
        <input id="pic_file" type="file" name="pic[]" multiple="multiple" accept="image/*"><br>
      </div>
      <label>商品説明</label>
      @error('content')
        <span class="text-danger font-weight-bold ml-5">{{$message}}</span><br>
      @enderror
      <textarea name="content" class="form-control">{{old('content') ?? $items->content}}</textarea>
      <div class="col-sm-4">
        <label>カラー</label><br>
        <input type="text" class="form-control" name='color' value="{{old('color') ?? $items->color}}"><br>
      </div>
      <label>詳細</label><br>
      @error('stock')
        <span class="text-danger font-weight-bold ml-5">{{$message}}</span><br>
      @enderror
      @foreach($details as $detail)
        <input type="hidden" name="product_detail_id[]" value="{{$detail->product_detail_id}}">
        <div class="form-group row">
          <div class="col-sm-4">
            <label>サイズ{{$loop->iteration}}</label><br>
            <input type="text" class="form-control" name='size[]' value="{{old('size') ?? $detail->size}}"><br>
          </div>
          <div class="col-sm-4">
            <label>在庫数{{$loop->iteration}}</label><br>
            <input type="number" class="form-control" name='stock[]' value="{{old('stock') ?? $detail->stock}}"><br>
          </div>      
        </div>
      @endforeach
      <br>
      <label>販売価格</label>
      @error('unit_price')
        <span class="text-danger font-weight-bold ml-5">{{$message}}</span><br>
      @enderror
      <div class="col-sm-2">
        <input type="text" class="form-control" name='unit_price' value="{{old('unit_price') ?? $items->unit_price}}"><br>
      </div>
      <label>非公開</label>
      <input type="hidden" name='status' value="1">
      <input type="checkbox" class="checkItem" name='status'  id= 'status' value="0"
        @if ((old ('status') ?? $items->status)==0)
          checked="checked"
        @endif
      ><br>
      <div id='release_data'
        @if ($items->status!=0)
          style="display:none;"
        @endif
      >
        @error('date')
          <span class="text-danger font-weight-bold">{{$message}}</span><br>
        @enderror
        @error('time')
          <span class="text-danger font-weight-bold">{{$message}}</span><br>
        @enderror
        <label>公開日時</label>
        <input type="date" name='date' value="{{old('date') ?? $date}}">
        <input type="time" name='time' value="{{old('time') ?? $time}}"><br>
      </div>
      <input type="submit" value="登録">
    </form>
  </div>
  <script type="text/javascript" src="{{ asset('assets/js/productEdit.js') }}">  </script>
@endsection