@extends('layouts.adminapp')
@section('title', 'product management')
@section('content')
  <h1 class="text-center">商品管理</h1>
  <div class = "flex">
    <form method="POST" action="product_search"><!--商品検索-->
      @csrf
      <input type="text" name="product_search" value="{{$product_search = ''}}"
      placeholder="商品名">
      <input type="submit" value="検索">
    </form>
    <button id="register">新規登録</button><!-- 商品登録 -->
    <button id="productDel">削除</button><!-- 削除 -->
  </div>
  <form id="form" name="form" method="POST" action="{{ url('/product_delete') }}"><!--削除-->
    @csrf
    <table class="table table-hover table-fixed">
      <thead>
        <tr>
          <th>✔ </th>
          <th>商品ID</th>
          <th>商品名</th>
          <th>画像</th>
          <th>商品説明</th>
          <th>サイズ</th>
          <th>カラー</th>
          <th>単価</th>
          <th>消費税</th>
          <th>税込み価格</th>
          <th>在庫数</th>
          <th>公開状態</th>
          <th>公開予定日</th>
          <th>登録日</th>
          <th>更新日</th>
        </tr>
      </thead>
      <tbody>
        @foreach($items as $item)
          <tr>
            <td class="text-center"><input type="checkbox" class="checkItem" id="checkItem" name="check[{{$loop->index}}]" value="{{$item->product_id}}"></td>
            <td onclick="Edit('{{$item->product_id}}')" name="producid" class = "td01">{{$item->product_id}}</td>
            <td onclick="Edit('{{$item->product_id}}')" name="productname" class = "td02">{{$item->product_name}}</td>
            <td onclick="Edit('{{$item->product_id}}')" name="pic" class = "td03">
              @if(empty($item->pic))
                <img src="/storage/no-image.png" alt="No Image" style="width: 100px; height: auto;">
              @else
                <img src="/storage/{{$item->pic}}" alt="{{$item->product_id}}" style="width: 100px; height: auto;">
              @endif
            </td>
            <td onclick="Edit('{{$item->product_id}}')" name="content" class = "td04">{{$item->content}}</td>
            <!-- サイズと在庫の呼び出し -->
            @foreach($item->productDetailModel as $details)
            <td onclick="Edit('{{$item->product_id}}')" name="size" class = "td05">{{$details->size}},{{$details->stock}}</td>
            @endforeach
            <td onclick="Edit('{{$item->product_id}}')" name="unit_price" class = "td07">{{$item->unit_price}}</td>
            <td onclick="Edit('{{$item->product_id}}')" name="tax" class = "td08">{{$item->tax}}</td>
            <td onclick="Edit('{{$item->product_id}}')" name="price" class = "td09">{{$item->price}}</td>
            <td onclick="Edit('{{$item->product_id}}')" name="status" class = "td10">{{$item->color}}</td>
            <td onclick="Edit('{{$item->product_id}}')" name="status" class = "td11">{{$item->status}}</td>
            <td onclick="Edit('{{$item->product_id}}')" name="release_date" class = "td012">{{$item->release_date}}</td>
            <td onclick="Edit('{{$item->product_id}}')" name="created_at" class = "td013">{{$item->created_at}}</td>
            <td onclick="Edit('{{$item->product_id}}')" name="update_at" class = "td014">{{$item->update_at}}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {{ $items ?? ''->links() }}
  </form>
  <script type="text/javascript" src="{{ asset('assets/js/product.js') }}"></script>
@endsection
