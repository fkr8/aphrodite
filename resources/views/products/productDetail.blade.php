@extends('layouts.usersapp')
@section('title', 'product Detail')
@section('head')
  <!-- slick読み込み -->
  <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick-theme.css') }}" media="screen" />
  <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick.css') }}" media="screen" />
  <script type="text/javascript" src="{{ asset('slick/slick.min.js') }}"></script>
@endsection
@section('content')
  <h1 class="text-center">{{$items['items']->product_name}}</h1>
  @if(empty($items['pics']))
    <img src="{{ asset('/assets/images/noimage.png') }}" alt="No Image" style="width: auto; height: 300px;">
  @else
    <div class="disp">
      <button id="cancel" class="btn btn-secondary btn-lg">✖</button>
      <div class="big">
        @foreach($items['pics'] as $pic)
          <figure><img src="/storage/{{$pic}}"></figure>
        @endforeach
      </div>
      <!-- 左右矢印の変更でうまく動作しない -->
      <!-- <div id="arrows">
        <div class="slick-prev">
          <img src="asset('slick/left.png')" alt="→">
        </div>
        <div class="slick-next">
          <img src="asset('slick/right.png')" alt="←">
        </div>
      </div> -->
    </div>
    <div class="d-flex">
      <div class="slider">
        @foreach($items['pics'] as $pic)
          <figure><img src="/storage/{{$pic}}"></figure>
        @endforeach
      </div>
      <div class="thumb">
        @foreach($items['pics'] as $pic)
          <figure><img src="/storage/{{$pic}}"></figure>
        @endforeach
      </div>
    </div>
  @endif
  <div>
    <p>{{$items['items']->content}}</p>
  </div>
  <div>
    <p>
    ￥{{number_format($items['items']->price)}}
    </p>
    <p>※こちらの商品には消費税が含まれています。</p>
    <p>※送料は別途発生いたします。詳細は<a href="">こちら</a></p>
  </div>
  @if($items['items']->del_flg==1 || $items['items']->status==0)
    <p>この商品は管理者によって非公開または削除されました。</p>
  @else
    <form action="{{url('/cart')}}" method="post">
      @csrf
      <input type="hidden" name="productId" value="{{$items['items']->product_id}}">
      
      <!-- カラー-->
        <p>COLOR：{{$items['items']->color}}</p>
        <!-- サイズ -->
        <label>SIZE:</label>
        <select name="size">
        @foreach ($items['items']->productDetailModel as $details)
          <option value="{{$details->size}}">{{$details->size}}</option>
        @endforeach
        </select>
        <label>数量:</label>
        <select name="quantity">
          @for($i=1;$i<=50;$i++)
            <option value="{{$i}}">{{$i}}</option>
          @endfor
        </select><br>
      <input type="submit" class="btn btn-lg btn-dark" value="ADD TO CART">
    </form>
  @endif
  <script type="text/javascript" src="{{ asset('assets/js/productDetail.js') }}"></script>
@endsection
