@extends('layouts.adminapp')
@section('title', 'product Regist')
@section('content')
  <h1 class="text-center">商品登録</h1>
  <div class="container">
    <form method="POST" action="product_regist" class="form-group" enctype="multipart/form-data">
      @csrf
      <label>商品名</label>
      @error('product_name')
        <span class="text-danger font-weight-bold ml-5">{{$message}}</span><br>
      @enderror
      <input type="text" class="form-control" name='product_name' value="{{old('product_name')}}"><br>
      <label>商品画像</label>
      <div class="mb-3">
        <div id="pic_area" style="width: 100%; height: 200px; display: flex; justify-content: center; border: 1px solid #ced4da; position: relative;">
          <div class="pic_box_ini" style="width: auto; height: 100%;">
            <img id="no_image" src="{{ asset('/assets/images/noimage.png') }}"alt="No Image" style="width: 100px; height: auto;">
          </div>
        </div>
        枠内に画像をドラッグ&ドロップ　または　
        <input id="pic_file" type="file" name="pic[]" multiple="multiple" accept="image/*">
      </div>
      <label>商品説明</label>
      @error('content')
        <span class="text-danger font-weight-bold ml-5">{{$message}}</span><br>
      @enderror
      <textarea name="content" class="form-control">{{old('content')}}</textarea><br>
      <div class="col-sm-4">
        <label>カラー</label><br>
        <input type="text" class="form-control" name='color'><br>
      </div>
      <label>詳細</label>
      @error('stock')
        <span class="text-danger font-weight-bold ml-5">{{$message}}</span><br>
      @enderror      
      <section class="select">
        <input type="button" value="＋" class="add pluralBtn">
        <input type="button" value="－" class="del pluralBtn">
        <input type="hidden" name="product_id">
        <div class="form-group row">
          <div class="col-sm-4">
            <label>サイズ</label><br>
            <input type="text" class="form-control" name='size[]'><br>
          </div>
          <div class="col-sm-4">
            <label>在庫数</label><br>
            <input type="text" class="form-control" name='stock[]'><br>
          </div>      
        </div>
      </section>
      
      <label>販売価格</label>
      @error('unit_price')
        <span class="text-danger font-weight-bold ml-5">{{$message}}</span><br>
      @enderror
      <div class="col-sm-2">
        <input type="text" class="form-control" name='unit_price' value="{{old('unit_price')}}"><br>
      </div>
      <label>非公開</label>
      <input type="hidden" name='status' value="1" >
      <input type="checkbox" class="checkItem" name='status' id='status' value="0"
        @if(old('status')==0)
          checked="checked"
        @endif
      ><br>
      <div id='release_data'>
        @error('date')
          <span class="text-danger font-weight-bold">{{$message}}</span><br>
        @enderror
        @error('time')
          <span class="text-danger font-weight-bold">{{$message}}</span><br>
        @enderror
        <label>公開日時</label>
        <input type="date" name='date' value="{{old('date')}}">
        <input type="time" name='time' value="{{old('time')}}"><br>
      </div>
      <input type="submit" value="登録">
    </form>
  </div>
  <script type="text/javascript" src="{{ asset('assets/js/productEdit.js') }}">  </script>
@endsection
