@extends('layouts.usersapp')
@section('title', 'Log in')
@section('head')
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
@endsection
@section('content')

<div class="form-group">
        
    <div class="login-logo">
        <a href="">ログイン画面</a>
    </div>

    <p class="login-box-msg">ログインして下さい。</p>

    @isset($msg)
    <p class="alert-danger">{{$msg}}</p>
    @endisset

    <form action="login" method="POST">
        <!-- メール -->
        <lavel for="email">メールアドレス</label>
            <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}">

        <!-- パスワード -->
        <lavel for="password">パスワード</label>
            <input type="password" id="password" name="password" class="form-control">
        <!--入力エラー-->
        @if ($errors->any())
        <div class="alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        
        <!-- ボタン-->
        <button type="submit" name="login" class="btn btn-dark">ログイン</button>
        {{ csrf_field() }}
    </form>
  <a class="regist" href="{{url('/regist')}}">会員登録はこちら</a>
</div>
@endsection