@extends('layouts.usersapp')
@section('title', 'cart Purchase')
@section('content')
  <h1 class="text-center">購入確認</h1>
  @if(empty($items))
    <div>現在、カート内に商品はありません。</div>
    <a href="{{url('/user_home')}}">HOMEへ</a>
  @else
    <table class="table table-hover table-fixed">
      <thead>
        <tr>
          <th>商品画像</th>
          <th>商品名</th>
          <th>サイズ</th>
          <th>単価</th>
          <th>数量</th>
          <th>小計</th>
        </tr>
      </thead>
      <tbody>
        @foreach($items as $key=>$item)
          <tr>
            <!-- 画像 -->
            <td class="text-center">
              @if(empty($item['pic']))
                <img src="/storage/no-image.png" alt="No Image" style="width: 100px; height: auto;">
              @else
                <img src="/storage/{{$item['pic']}}" alt="{{$item['product_name']}}" style="width: 100px; height: auto;">
              @endif
            </td>
            <!-- 商品名 -->
            <td><a href="product_detail/{{$item['product_id']}}">{{$item['product_name']}}</a></td>
            <!-- サイズ -->
            <td>
              @if(!empty($item['size']))
                {{$item['size']}}
              @else
                -
              @endif
            </td>
            <!-- 単価 -->
            <td class="price" data-price="{{$item['price']}}">
              ￥{{number_format($item['price'])}}
            </td>
            <!-- 数量 -->
            <td>
              <select class="quantity" name="quantity" data-product_id="{{$item['product_id']}}">
                @for($i=1;$i<=100;$i++)
                  <option value="{{$i}}"
                    @if($i==$item['quantity'])
                      selected
                    @endif
                  >{{$i}}</option>
                @endfor
              </select>
              <input class="del-btn" type="button" value="削除" data-product_id="{{$item['product_id']}}">
            </td>
            <!-- 小計 -->
            <td class="sum-price" data-sum-price="{{$item['price']*$item['quantity']}}">
              ￥{{number_format($item['price']*$item['quantity'])}}
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="all-price font-weight-bold text-right mr-4"></div>
    <div class="text-center">
      <a href="{{url('/user_home')}}">HOMEへ</a>
      <a href="{{url('/purchase_complete')}}">購入</a>
    </div>
  @endif
  <script type="text/javascript" src="{{ asset('assets/js/cart.js') }}"></script>
@endsection
