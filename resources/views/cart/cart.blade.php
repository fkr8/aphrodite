@extends('layouts.usersapp')
@section('title', 'cart')
@section('content')
  <h1 class="text-center">カートの中身</h1>
  @if(empty($items))
    <div>現在、カート内に商品はありません。</div>
    <a href="{{url('/user_home')}}">HOMEへ</a>
  @else
    <table class="table table-hover table-fixed">
      <thead>
        <tr>
          <th>商品画像</th>
          <th>商品名</th>
          <th>カラー</th>
          <th>サイズ</th>
          <th>単価</th>
          <th>数量</th>
          <th>小計</th>
        </tr>
      </thead>
      <tbody>
       @foreach($items as $key=>$item)
         @if(!($item['del']==1 || $item['status']==0))
            @if(session()->has('cart_data'))
            <!-- セッション情報をループして出力 -->
                <tr>
                  <!-- 画像 -->
                  <td class="text-center">
                    @if(empty($item['pic']))
                      <img src="/storage/no-image.png" alt="No Image" style="width: 100px; height: auto;">
                    @else
                      <img src="/storage/{{$item['pic']}}" alt="{{$item['product_name']}}" style="width: 100px; height: auto;">
                    @endif
                  </td>
                  <!-- 商品名 -->
                  <td class="product_name">
                    {{$item['product_name']}}
                  </td>
                  <!-- カラー -->
                  <td class="color">
                    {{$item['color']}}
                  </td>
                  <!-- サイズ -->
                  <td class="size">
                    {{$item['size']}}
                  </td>
                  <!-- 単価 -->
                  <td class="unit-price">
                    {{$item['unit_price']}}
                  </td>
                  <!-- 数量 -->
                  <td>
                    <select class="quantity" name="quantity" data-detail_id="{{$item['product_detail_id']}}">
                      @for($i=1;$i<=100;$i++)
                        <option value="{{$i}}"
                          @if($i==$item['quantity'])
                            selected
                          @endif
                        >{{$i}}</option>
                      @endfor
                    </select>
                    <input class="del-btn" type="button" value="削除" data-detail_id="{{$item['product_detail_id']}}">
                  </td>
                  <!-- 小計 -->
                  <td class="sum-price" data-sum-price="{{$item['price']*$item['quantity']}}">
                  ￥{{number_format($item['price']*$item['quantity'])}}
                  </td>
                </tr>
            @endif



      <!-- カートレコードの中身を出力 以下途中-->

            <tr>
              <!-- 画像 -->
              <td class="text-center">
                @if(empty($item['pic']))
                  <img src="/storage/no-image.png" alt="No Image" style="width: 100px; height: auto;">
                @else
                  <img src="/storage/{{$item['pic']}}" alt="{{$item['product_name']}}" style="width: 100px; height: auto;">
                @endif
              </td>
              <!-- 商品名 -->
              <td><a href="product_detail/{{$item['product_id']}}">{{$item['product_name']}}</a></td>
              <!-- カラー -->
              <td>
                @if(!empty($item['color']))
                  {{$item['color']}}
                @else
                  -
                @endif
              </td>
              <!-- サイズ -->
              <td>
                @if(!empty($item['size']))
                  {{$item['size']}}
                @else
                -
                @endif
              </td>
              <!-- 単価 -->
              <td class="price" data-price="{{$item['price']}}">
                ￥{{number_format($item['price'])}}
              </td>
              <!-- 数量 -->
              <td>
                <select class="quantity" name="quantity" data-detail_id="{{$item['product_detail_id']}}">
                  @for($i=1;$i<=100;$i++)
                    <option value="{{$i}}"
                      @if($i==$item['quantity'])
                        selected
                      @endif
                    >{{$i}}</option>
                  @endfor
                </select>
                <input class="del-btn" type="button" value="削除" data-detail_id="{{$item['product_detail_id']}}">
              </td>
              <!-- 小計 -->
              <td class="sum-price" data-sum-price="{{$item['price']*$item['quantity']}}">
                ￥{{number_format($item['price']*$item['quantity'])}}
              </td>
            </tr>
          @endif
        @endforeach
      </tbody>
    </table>
    @foreach($items as $key=>$item)
      @if($item['del']==1 || $item['status']==0)
        <div>以下の商品は管理者により非公開または削除されました。</div>
        <div class="d-flex">
          @if(empty($item['pic']))
            <img src="/storage/no-image.png" alt="No Image" style="width: 100px; height: auto;">
          @else
            <img src="/storage/{{$item['pic']}}" alt="{{$item['product_name']}}" style="width: 100px; height: auto;">
          @endif
          <div class="mx-2">商品名：<a href="product_detail/{{$item['product_id']}}">{{$item['product_name']}}</a></div>
          @if(!empty($item['size']))
            <div class="mx-2">サイズ：{{$item['size']}}</div>
          @endif
          <div class="price mx-2" data-price="{{$item['price']}}">単価：￥{{number_format($item['price'])}}</div>
          <div class="mx-2">数量：{{$item['quantity']}}</div>
        </div>
      @endif
    @endforeach
    <div class="all-price font-weight-bold text-right mr-4"></div>
    <div class="text-center">
      <a href="{{url('/user_home')}}">HOMEへ</a>
      @if(session()->has('ses_data'))
        <a href="{{url('/purchase')}}">購入画面へ</a>
      @else
        <a href="{{url('/login')}}">ログインして購入</a>
      @endif
    </div>
  @endif
  <script type="text/javascript" src="{{ asset('assets/js/cart.js') }}"></script>
@endsection
