@extends('layouts.adminapp')
@section('title', 'management home')
@section('head')
<!-- slick読み込み -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
@endsection
@section('content')
<h1 class="text-center">HOME</h1>
<table class="table table-hover table-fixed">
    <thead>
        <tr>
            <th>今月</th>
            <th>今日</th>
            <th>昨日</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td name="this_month_sales" class="td01">{{$items['salesCondition']['monthSales']}}</td>
            <td name="today_sales" class="td02">{{$items['salesCondition']['todaySales']}}</td>
            <td name="yesterday_sales" class="td03">{{$items['salesCondition']['yesterdaySales']}}</td>
        </tr>
    </tbody>
</table>

<div class="d-flex justify-content-center">
    <form action="">
        @if($items['dispGraph'] == "weekly")
        <button class="bg-secondary text-white" disabled type="submit" name="graph" value="weekly">週間</button>
        <button type="submit" name="graph" value="yearly">年間</button>
        @elseif($items['dispGraph'] == "yearly")
        <button type="submit" name="graph" value="weekly">週間</button>
        <button class="bg-secondary text-white" disabled type="submit" name="graph" value="yearly">年間</button>
        @else
        $dispGraphが{{$items['dispGraph']}}
        @endif
    </form>
</div>


<canvas id="stage"></canvas>

</body>

<script>
    window.Laravel = {};
    window.Laravel.date = JSON.parse('{!! json_encode($items["salesGraph"]["date"]) !!}');
    window.Laravel.sales = JSON.parse('{!! json_encode($items["salesGraph"]["sales"]) !!}');
</script>
<script type="text/javascript" src="{{ asset('assets/js/home.js') }}"></script>
@endsection