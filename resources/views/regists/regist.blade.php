@extends('layouts.usersapp')
@section('title', 'Regist')
@section('head')
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
@endsection
@section('content')
<div class="form-group">

  <div class="regist-logo">
    <a href="">会員登録画面</a>
  </div>

  <p class="regist-box-msg">会員登録フォーム</p>

  <form action="regist" method="POST">
      
      <lavel for="name">氏名</label>
        <input type="name" id="name" name="name" class="form-control" value="{{ old('name') }}">
      <lavel for="zip">郵便番号</label>
        <input type="zip" id="zip" name="zip" class="form-control" value="{{ old('zip') }}" placeholder="ハイフン不要">
      <lavel for="address">住所</label>
        <input type="address" id="address" name="address" class="form-control" value="{{ old('address') }}">
      <lavel for="tel">電話番号</label>
        <input type="tel" id="tel" name="tel" class="form-control" value="{{ old('tel') }}" placeholder="ハイフン不要">
      <lavel for="age">年齢</label>
        <input type="age" id="age" name="age" class="form-control" value="{{ old('age') }}">
      <lavel for="email">メールアドレス＝ログインID</label>
        <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}">
      <lavel for="email">メールアドレス（確認）</label>
        <input type="email" id="email_confirmation" name="email_confirmation" class="form-control">
      <lavel for="password">パスワード</label>
        <input type="password" id="password" name="password" class="form-control">
      <lavel for="password">パスワード（確認）</label>
        <input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
        <input type="hidden" name="created_at" value="{{date('Y-m-d H:i:s')}}">
         
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

      <!-- ボタン-->
      <button type="submit" name="regist" class="btn btn-dark">会員登録</button>
      {{ csrf_field() }}
  </form>
</div>
@endsection