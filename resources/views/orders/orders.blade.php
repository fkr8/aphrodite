@extends('layouts.adminapp')
@section('title', 'order management')
@section('content')
<h1 class="text-center">受注管理</h1>
<div class="flex">
    <form id="form" name="form" method="POST" action="orderDelete" class="orderDel">
        <!--削除-->
        @csrf
        <input type="submit" id="orderDel" onclick="return Delete();" value="削除">
</div>
<table class="table table-hover table-fixed">
    <thead>
        <tr>
            <th>✔</th>
            <th>注文ID</th>
            <th>注文者</th>
            <th>注文日時</th>
            <th>商品名</th>
            <th>数量</th>
            <th>税込み金額</th>
            <th>送料</th>
            <th>合計金額</th>
        </tr>
    </thead>
    <tbody>
        @foreach($items as $item)
        <tr>
            <td class="text-center"><input type="checkbox" class="checkItem" id="checkItem" name="check[{{$loop->index}}]" value="{{$item->order_id}}"></td>
            <td name="order_id" class="td01">{{$item->order_id}}</td>
            <td name="name" class="td02">{{$item->orderStatementModel->userModel->name}}</td>
            <td name="order_date" class="td03">{{$item->orderStatementModel->order_date}}</td>
            <td name="about" class="td04">{{$item->product_id}}</td>
            <td name="order_id" class="td01">{{$item->quantity}}</td>
            <td name="name" class="td02">{{$item->showPrice("unit_price")}}</td>
            <td name="order_date" class="td03">{{$item->showPrice("tax")}}</td>
            <td name="about" class="td04">{{$item->showPrice("price")}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</form>

<script>
    //削除
    function Delete() {
        if (window.confirm('選択された行を削除します。よろしいですか？')) { // 確認ダイアログを表示
            $('#form').prop('action', '/orderDelete');
            $('#form').submit(); // 確認ダイアログを表示
            return true; // 「OK」時は送信を実行
        } else { // 「キャンセル」時の処理
            //window.alert('キャンセルされました'); // 警告ダイアログを表示
            return false; // 送信を中止
        }
    }
</script>
@endsection