@extends('layouts.usersapp')
@section('title', 'contact comfirm')
@section('content')
  <h1 class="text-center">お問い合わせ内容確認</h1>
  <div>
    <p>お名前</p>
    <p>{{$name}}</p>
    <p>メールアドレス</p>
    <p>{{$mail}}</p>
    <p>件名</p>
    <p>{{$subject}}</p>
    <p>お問い合わせ内容</p>
    <div>{{$content}}</div>
  </div>
  <form action="{{url('/contact_send')}}" method="post">
    @csrf
    <input type="submit" value="送信">
  </form>
@endsection
