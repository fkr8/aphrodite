@extends('layouts.usersapp')
@section('title', 'contact')
@section('content')
  <h1 class="text-center">お問い合わせ</h1>
    <div class="form-group">
      <form action="{{url('/contact_comfirm')}}" method="post">
        @csrf
        <label>お名前</label>
        @error('name')
          <span class="text-danger font-weight-bold ml-5">{{$message}}</span><br>
        @enderror
        <input type="text" class="form-control" name='name' value="{{old('name')}}"><br>
        <label>メールアドレス</label>
        @error('mail')
          <span class="text-danger font-weight-bold ml-5">{{$message}}</span><br>
        @enderror
        <input type="email" class="form-control" name='mail' value="{{old('mail')}}"><br>
        <label>件名</label>
        <select name="subject" class="form-control">
          <option value="0" {{ old('subject')==0 ? "selected":"" }}>商品について</option>
          <option value="1" {{ old('subject')==1 ? "selected":"" }}>ライブブッキング</option>
          <option value="2" {{ old('subject')==2 ? "selected":"" }}>その他</option>
        </select><br>
        <label>お問い合わせ内容</label>
        @error('content')
          <span class="text-danger font-weight-bold ml-5">{{$message}}</span>
        @enderror  
        <textarea name="content" class="form-control" rows="4" cols="40">{{old('content')}}</textarea><br>
        <input type="submit" class="btn btn-dark" value="確認">
      </form>
    </div>
@endsection
