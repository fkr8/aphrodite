@extends('layouts.usersapp')
@section('title', 'user Home')
@section('userHome_content')
<div class="container">
    <div class="row">
        @foreach($items as $item)
        <div class="col-lg-3">
            <div id="pic_area" style="width: 100%; height: 150px; justify-content: center; border: 1px solid #ced4da; position: relative;">
                <div class="pic_box_ini" style="width: auto; height: 100%;">
                    <a href="/product_detail/{{$item->product_id}}">
                        @if(empty($item->pic))
                        <img src="{{ asset('/assets/images/noimage.png') }}" alt="No Image" style="width: 100px; height: auto;">
                        @else
                        <img src="/storage/{{$item->pic}}" alt="{{$item->pic}}" style="width: 100px; height: auto;">
                        @endif
                    </a>
                </div>
            </div>
            <a href="/product_detail/{{$item->product_id}}">
                <div class="text-dark text-center">
                    <h2 class="font-small">{{$item->product_name}}</h2>
                        @if ($item->color != null)
                        <h2 class="font-small">COLOR:{{$item->color}}</h2>
                        @endif
                    @foreach($item->productDetailModel as $details)
                      @if($details->stock == 0)
                      <h2 class="sold">SOLD OUT</h2>
                      @endif
                    @endforeach
                </div>
                <div class="text-dark text-center">
                    <h2 class="font-small">{{$item->price}}</h2>
                </div>
            </a><br>
        </div>
        @endforeach
    </div>
    <div class="pagination justify-content-center">
        {{$items->links()}}
    </div>
</div>
@endsection