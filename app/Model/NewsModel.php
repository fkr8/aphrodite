<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;


class NewsModel extends Model
{
    //
    protected $table;
    protected $db;

    public function __construct()
    {
        $this->table = 'm_about';
        $this->db = DB::connection();
    }

    //about表示
    public function getAbout()
    {
        $items = $this->db->table($this->table)
        ->select('about')
        ->where('del_flg','=',null)
        ->orderBy('about_id', 'desc')
        ->get();
        return $items;
    }
}