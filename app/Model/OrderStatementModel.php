<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderStatementModel extends Model
{
    protected $table = 't_order_statements';
    protected $db;

    public function __construct()
    {
        $this->db = DB::connection();
    }

    // ユーザーモデルの呼び出し
    public function userModel()
    {
        return $this->belongsTo('App\Model\UserModel', 'user_id', 'user_id');
    }
}
