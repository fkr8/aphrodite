<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class CartModel extends Model
{
    protected $table = 't_cart';
    protected $primaryKey = 'cart_id';
    protected $db;

    public function __construct()
    {
        $this->db = DB::connection();
    }

    // 商品詳細モデルの呼び出し
    public function productDetailModel()
    {
        return $this->belongsto('App\Model\ProductDetailModel', 'product_detail_id', 'product_detail_id');
    }

    // カート内の商品~まずカート呼び出し　商品IDのdelflgがnullまたはstatusが1を取得。
    public function cartItems($userId)
    {
        $cart = $this
        ->where('user_id',$userId)
        ->join('t_product_details','t_cart.product_detail_id','=','t_product_details.product_detail_id')
        ->join('m_products','m_products.product_id','=','t_product_details.product_id')
        ->whereNull('m_products.del_flg')
        ->orWhere('m_products.status','=',1)
        ->get();

        $items = [];
        foreach($cart as $item){
            $items[] = [
                'product_id' => $item['product_id'],
                'del' => $item['m_products.del_flg'],
                'product_name' => $item['product_name'],
                'pic' => explode(',', $item['pic'])[0],
                'unit_price' => $item['unit_price'],
                'tax' => $item['tax'],
                'price' => $item['price'],
                'size' => $item['size'],
                'status' => $item['status'],
                'quantity' => $item['quantity'],
            ];
            // 管理者による非公開or削除 R4.1.31作成中
            // if($item->productModel->del_flg==1 || $item->productModel->status==0){
            //     $this->db->beginTransaction();
            //     try{
            //         $this->db->table($this->table)
            //             ->where([
            //                 ['user_id', $userId],
            //                 ['product_id', $item->productModel->product_id],
            //             ])
            //             ->update([
            //                 'del_flg' => 1,
            //                 'quantity' => 0,
            //                 'update_at' => date("Y-m-d H:i:s"),
            //             ]);
            //         $this->db->commit();
            //     } catch (\Exception $e) {
            //         $this->db->rollback();
            //         echo '<script type="text/javascript">';
            //         echo 'alert("エラーが発生しました。カート確認処理は失敗しました。");';
            //         echo 'location.href="/cart"';
            //         echo '</script>';
            //         exit;
            //     }
            // }
        }
        return $items;
    }

    // カート商品追加
    public function cartAdd($userId,$details)
    {
        $isData = $this
            ->where([
                ['user_id', $userId],
                ['product_detail_id', $details['product_detail_id']],
            ])
            ->exists();
        $this->db->beginTransaction();
        try{
            if($isData) {
                $this->db->table($this->table)
                    ->where([
                        ['user_id', $userId],
                        ['product_detail_id', $details['product_detail_id']],
                    ])
                    ->increment('quantity', $details['quantity']);
                $this->db->table($this->table)
                    ->where([
                        ['user_id', $userId],
                        ['product_detail_id', $details['product_detail_id']],
                    ])
                    ->update([
                        'del_flg' => null,
                        'update_at' => date("Y-m-d H:i:s"),
                    ]);
            } elseif(!$isData) {
                $data = [
                    'user_id' => $userId,
                    'product_detail_id' => $details['product_detail_id'],
                    'quantity' => $details['quantity'],
                    'created_at' => date("Y-m-d H:i:s"),
                    'update_at' => date("Y-m-d H:i:s"),
                ];
                $this->db->table($this->table)->insert($data);
            }
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollback();
            echo '<script type="text/javascript">';
            echo 'alert("エラーが発生しました。商品追加処理は失敗しました。");';
            echo 'location.href="/cart"';
            echo '</script>';
            exit;
        }
    }

    // カート内商品削除
    public function cartDelete($userId, $detailId)
    {
        $this->db->beginTransaction();
        try{
            $this->db->table($this->table)
                ->where([
                    ['user_id', $userId],
                    ['product_detail_id', $detailId],
                ])
                ->update([
                    'del_flg' => 1,
                    'quantity' => 0,
                    'update_at' => date("Y-m-d H:i:s"),
                ]);
            $this->db->commit();
        } catch (\Exception $e) {
            var_dump($e);
            $this->db->rollback();
            echo '<script type="text/javascript">';
            echo 'alert("エラーが発生しました。削除処理は失敗しました。");';
            echo 'location.href="/cart"';
            echo '</script>';
            exit;
        }
    }

    //カート内数量変更
    public function cartChangeQuantity($userId, $detailId, $quantity)
    {
        $this->db->beginTransaction();
        try{
            $this->db->table($this->table)
                ->where([
                    ['user_id', $userId],
                    ['product_detail_id', $detailId],
                ])
                ->update([
                    'quantity' => $quantity,
                    'update_at' => date("Y-m-d H:i:s"),
                    ]);
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollback();
            echo '<script type="text/javascript">';
            echo 'alert("エラーが発生しました。 変更処理は失敗しました。");';
            echo 'location.href="/cart"';
            echo '</script>';
            exit;
        }
    }

    //カート内購入確認 ~まずカート呼び出し　商品IDのdelflgがnullまたはstatusが1を取得。
    public function cartPurchase($userId)
    {
        $cart = $this
                    ->where('user_id',$userId)
                    ->join('t_product_details','t_cart.product_detail_id','=','t_product_details.product_detail_id')
                    ->join('m_products','m_products.product_id','=','t_product_details.product_id')
                    ->whereNull('m_products.del_flg')
                    ->orWhere('m_products.status','=',1)
                    // ->select('t_product_details.size','m_products.price','m_products.pic')
                    ->get();

        foreach($cart as $item){
                $items[] = [
                    'product_id' => $item['product_id'],
                    'product_name' => $item['product_name'],
                    'pic' => explode(',', $item['pic'])[0],
                    'unit_price' => $item['unit_price'],
                    'tax' => $item['tax'],
                    'price' => $item['price'],
                    'size' => $item['size'],
                    'quantity' => $item['quantity'],
                ];
        }
        return $items;
    }

    //カート内購入処理
    public function purchaseComplete($userId)
    {
        $cart = $this->with('productModel')
            ->where('user_id', $userId)
            ->whereNull('del_flg')
            ->get();
        $allPrice = 0; //合計金額を計算
        $orderItems = []; //$orderItemsをt_orderへ保存
        foreach($cart as $item){
            $orderItems[] = [
                'product_id' => $item->productModel->product_id,
                'quantity' => $item->quantity,
                'unit_price' => $item->productModel->unit_price,
                'tax' => $item->productModel->tax,
                'price' => $item->productModel->price,
                'created_at' => date("Y-m-d H:i:s"),
                'update_at' => date("Y-m-d H:i:s"),
            ];
            if($item->quantity > $item->productModel->stock){
                echo '<script type="text/javascript">';
                echo 'alert("在庫が足りません");';
                echo 'location.href="/cart"';
                echo '</script>';
                exit;
            }
            $allPrice += $item->productModel->price * $item->quantity;
        }
        $orderStatements = [    //$orderStatementsをt_order_statementsへ保存
            'user_id' => $userId,
            'postage' => 500,   //仮の送料
            'all_price' => $allPrice,
            'order_date' => date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s"),
            'update_at' => date("Y-m-d H:i:s"),
        ];
        $this->db->beginTransaction();
        try{
            //カート内商品削除
            $this->db->table($this->table)
                ->where('user_id', $userId)
                ->delete();
            //商品ストック変更(m_products)
            foreach($orderItems as $item){
                $this->db->table('m_products')
                    ->where('product_id', $item['product_id'])
                    ->decrement('stock', $item['quantity']);
                $this->db->table($this->table)
                    ->where('product_id', $item['product_id'])
                    ->update(['update_at' => date("Y-m-d H:i:s")]);
            }
            //t_order_statementsへ保存(t_order_statements)
            $statementId = $this->db->table('t_order_statements')->insertGetId($orderStatements, 'statement_id');
            //t_ordersへ保存(t_orders)
            foreach($orderItems as $item){
                $item['statement_id'] = $statementId;
                $this->db->table('t_orders')->insert($item);
            }
            //コミット
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollback();
            echo '<script type="text/javascript">';
            echo 'alert("エラーが発生しました。 購入処理は失敗しました。");';
            echo 'location.href="/cart"';
            echo '</script>';
            exit;
        }
    }
}
