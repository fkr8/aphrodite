<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ContentModel extends Model
{
    //
    protected $table = 'm_about';
    protected $db;

    public function __construct()
    {
        $this->db = DB::connection();
    }
    //画面呼び出し
    public function contentIndex($request)
    {
        $items = $this->db->table('m_about')
            ->where('del_flg', null)->orderBy('about_id', 'asc')
            ->paginate(10);
        return $items;
    }

    //ABOUT新規登録
    public function contentCreate($request)
    {
        $items = [
            'about' => $request->about,
            'created_at' => $request->created_at,
        ];
        DB::table('m_about')->insert($items);
        return $items;
    }

    //編集画面の出力
    public function getContentEdit($request)
    {
        try {
            $aboutId = $request->input('about');
            $items = $this->db->table('m_about')
                ->where('about_id', $aboutId)
                ->first();
        } catch (Exception $e) {
            var_dump($e);
        }
        return $items;
    }

    //編集登録
    public function getContentUpdate($param)
    {
        try {
            $ret = $this->db->table('m_about')
                ->where('about_id', $param->about_id)
                ->update([
                    'about_id' => $param->about_id,
                    'about'    => $param->about,
                    'update_at' => $param->update_at,
                ]);
        } catch (Exception $e) {
            var_dump($e);
        }
        return;
    }

    //削除（論理削除処理）
    public function getContentDelete($aboutid)
    {
        try {
            $ret = $this->db->table('m_about')
                ->where('about_id', $aboutid)
                ->update([
                    'del_flg'              => 1,
                ]);
        } catch (Exception $e) {
            var_dump($e);
        }
        return;
    }
}

//   //削除（物理削除処理）
//   public function getaboutDelete($aboutid){
//     try {
//     $ret = $this->db->table('m_about')
//     ->where('about_id', $aboutid)
//     ->delete();
//     } catch (Exception $e) {
//       var_dump($e);
//     }
//     return;
//     }
// }
