<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserRegistModel extends Model
{
    //
    protected $table;
    protected $db;

    public function __construct()
    {
        $this->table = 'm_users';
        $this->db = DB::connection();
    }

    //ユーザ登録
    public function userRegist($request)
    {
        $hashed_pass = \Hash::make($request->password);

        $items = [
            'name' => $request->name,
            'zip' => $request->zip,
            'address' => $request->address,
            'tel' => $request->tel,
            'age' => $request->age,
            'email' => $request->email,
            'password' => $hashed_pass,
            'created_at' => $request->created_at,
        ];


        $this->db->beginTransaction();
        try {

            $this->db->table($this->table)
                ->insert($items);

            // return $regist;

        $this->db->commit();
        } catch (Exception $e) {
            //不明な失敗
            $this->db->rollback();
            echo '<script type="text/javascript">';
            echo 'alert("エラーが発生しました。処理は失敗しました。");';
            echo 'location.href="/regist"';
            echo '</script>';
            exit;
        }
    }
}
