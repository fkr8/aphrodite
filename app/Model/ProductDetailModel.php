<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductDetailModel extends Model
{
    //商品詳細の呼び出し
    protected $table = 't_product_details';
    protected $primaryKey = 'product_detail_id';
    protected $db;

    public function __construct()
    {
        $this->db = DB::connection();
    }

    // 商品モデルの呼び出し
    public function productModel()
    {
        return $this->belongsTo('App\Model\ProductModel', 'product_id');
    }
    
    // カートモデルの呼び出し
    public function cartModel()
    {
        return $this->hasMany('App\Model\CartModel', 'product_detail_id');
    }

    //商品詳細の編集登録
    public function productDetailUpdate($request)
    {
        //詳細
        $details = [
            // 'product_id'=>$request->product_id,
            'product_detail_id'=>$request->product_detail_id,//PK
            'size'=>$request->size,
            'stock'=>$request->stock,
            // 'update_at'=>date("Y-m-d H:i:s"),
        ];
    //詳細テーブルの一括更新
        if($details['product_detail_id'] != null) {
                $this->db->beginTransaction();
                try
                {
                    for ($i=0; $i < count($details['product_detail_id']); $i++) {

                        $this->db->table('t_product_details')
                            ->where('product_id',$request->product_id)
                            ->where('product_detail_id',$details['product_detail_id'][0]+$i)
                            ->update([
                                'size'  => $details['size'][$i],
                                'stock' => $details['stock'][$i],
                            ]);
                    }
                    // var_dump($details['stock']);  
                    $this->db->commit();
                } catch (\Exception $e) {
                    // var_dump($e);
                    $this->db->rollback();
                    echo '<script type="text/javascript">';
                    echo 'alert("エラーが発生しした。登録処理は失敗しました。");';
                    echo 'location.href="/product"';
                    echo '</script>';
                    exit;
                }
        }
    }

    //選択した商品詳細をカートへ　product_detail_idの取得
    public function cartDetailId($request)
    {
        if (!empty($request->size)){
            $detailId = $this
                ->where('size',$request->size)
                ->value('product_detail_id');
            return $detailId;
        }        
    }
}