<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductModel extends Model
{
    //商品テーブルの呼び出し
    protected $table = 'm_products';
    protected $primaryKey = 'product_id';
    protected $db;
    protected $taxRate = 0.1;

    public function __construct()
    {
        $this->db = DB::connection();
    }

    //商品詳細 product_detailsの呼び出し
    public function productDetailModel()
    {
        return $this->hasMany('App\Model\ProductDetailModel','product_id');
    }
    //カート（孫テーブル）の呼び出し
    public function cartModel()
    {
        return $this->hasManyThrough('App\Model\CartModel','App\Model\ProductDetailModel');
    }

    //商品一覧
    public function productIndex()
    {
        $items = $this->with('productDetailModel')
            ->where('del_flg', null)
            ->orderBy('product_id', 'asc')
            ->paginate(10);
        foreach ($items as $item) {
            if ($item->status == 1){ $item->status = '公開'; }
            else { $item->status = '非公開'; }
            $item->pic = explode(',', $item->pic)[0];
        }
        return $items;
    }
    
    //名前検索
    public function productSearch($request)
    {
        try {
             $productSearch = $request->product_search;
             $items = $this->with('productDetailModel')
                 ->where('del_flg', null)
                 ->where(function($query) use ($productSearch){
                     $query->orwhere('product_name', 'like', '%'.$productSearch.'%')
                           ->orwhere('content', 'like', '%'.$productSearch.'%');
                 })
                 ->orderBy('product_id','asc')
                 ->paginate(10);

             foreach ($items as $item) {
                 if ($item->status == 1){ $item->status = '公開'; }
                 else { $item->status = '非公開'; }
                 $item->pic = explode(',', $item->pic)[0];
             }
        } catch (Exception $e) {
            var_dump($e);
        }
        return $items;
    }

    //商品編集
    public function getProductEdit($id)
    {
        try{
            $items = $this->with('productDetailModel')
                ->where('product_id', $id)
                ->first();
            $details = $items->productDetailModel;
            $dateTime = date_create($items->release_date);
            $date = date_format($dateTime , 'Y-m-d');
            $time = date_format($dateTime , 'H:i:s');
            $pics = explode(',', $items->pic);
            array_pop($pics);
            $data = [
                'items' => $items,
                'date' => $date,
                'time' => $time,
                'pics' => $pics, 
                'details' => $details,
            ];
        } catch (Exception $e) {
            var_dump($e);
        }
        return $data;
    }

    //編集登録
    public function productUpdate($request)
    {
        $picsPath = "";
        if($request->has('pic_remine')){
            $pics = $request->pic_remine;
            foreach ($pics as $pic) {
                $picsPath = $picsPath.$pic.",";
            }
        }
        if($request->hasFile('pic')){
            $pics = $request->pic;
            foreach ($pics as $pic) {
                $path = $pic->store('public');
                $picsPath = $picsPath.basename($path).",";
            }
        }
        $tax = ($request->unit_price)*($this->taxRate);
        $price = $request->unit_price+$tax;
        $formData = [
            'product_name'=>$request->product_name,
            'pic' => $picsPath,
            'content'=>$request->content,
            'color'=>$request->color,
            'unit_price'=>$request->unit_price,
            'tax'=>$tax,
            'price'=>$price,
            'status'=>$request->status,
            'update_at'=>date("Y-m-d H:i:s"),
        ];
        // 公開日時の更新設定
        $status = $this->db->table('m_products')
            ->where('product_id', $request->product_id)
            ->value('status');
        if($request->status==0){
            $release = ($request->date)." ".($request->time);
            $formData['release_date'] = $release;
        } elseif ($status==0 && $request->status==1 || $request->status==1) {
            $formData['release_date'] = date("Y-m-d H:i:s");
        }

        $this->db->beginTransaction();
        try{
            $this->db->table('m_products')
                ->where('product_id', $request->product_id)
                ->update($formData);
        //詳細テーブルの更新はProductDetailModel
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollback();

            var_dump($e);
            // echo '<script type="text/javascript">';
            // echo 'alert("エラーが発生しました。編集処理は失敗しました。");';
            // echo 'location.href="/product"';
            // echo '</script>';
            // exit;
        }
    }

    //商品削除
    public function productDelete($checkList)
    {
        $this->db->beginTransaction();
        try{
            if(!empty($checkList)){
                foreach ($checkList as $product_id) {
                    $this->db->table($this->table)
                    ->where('product_id', $product_id)
                    ->update(['del_flg' => 1,]);
                }
                $this->db->commit();
            } else { //ここにスクリプト書くの?
                echo '<script type="text/javascript">';
                echo 'alert("チェックボックスを選択してから削除してください");';
                echo 'location.href="/product"';
                echo '</script>';
                exit;
            }
        } catch (\Exception $e) {
            $this->db->rollback();
            echo '<script type="text/javascript">';
            echo 'alert("エラーが発生しました。選択行削除処理は失敗しました。");';
            echo 'location.href="/product"';
            echo '</script>';
            exit;
        }
    }

    //商品登録
    public function productRegist($request)
    {
        $picsPath = "";
        if($request->hasFile('pic')){
            $pics = $request->pic;
            foreach ($pics as $pic) {
                $path = $pic->store('public');
                $picsPath = $picsPath.basename($path).",";
            }
        }
        $tax = ($request->unit_price)*($this->taxRate);
        $price = $request->unit_price+$tax;
        if($request->status==0){
            $release = ($request->date)." ".($request->time);
        } else {
            $release = date("Y-m-d H:i:s");
        }
        $formData = [
            'product_name'=>$request->product_name,
            'pic'=>$picsPath,
            'content'=>$request->content,
            'color'=>$request->color,
            'unit_price'=>$request->unit_price,
            'tax'=>$tax,
            'price'=>$price,
            'status'=>$request->status,
            'release_date'=>$release,
            'created_at'=>date("Y-m-d H:i:s"),
        ];
        //詳細
        $details = [
            'size'=>$request->size,
            'stock'=>$request->stock,
         ];

        $this->db->beginTransaction();
        try{
            $Id = $this->db->table('m_products')
                ->insertGetId($formData);
            $this->db->commit();
            //product_idの取得
            //詳細の登録
            for ($i=0; $i < count($details['stock']); $i++) {

                $this->db->table('t_product_details')
                    ->insert([
                        'product_id' =>$Id,
                        'size'  => $details['size'][$i],
                        'stock' => $details['stock'][$i],
                        'cart_id' => null,
                        'created_at' =>date("Y-m-d H:i:s"),
                    ]);
            }
        } catch (\Exception $e) {
            // var_dump($e);
            $this->db->rollback();
            echo '<script type="text/javascript">';
            echo 'alert("エラーが発生しました。登録処理は失敗しました。");';
            echo 'location.href="/product"';
            echo '</script>';
            exit;
        }
    }

    //商品一覧取得
    public function getProductList()
    {
        $items = $this->with('productDetailModel')
            ->select('product_id', 'product_name', 'pic', 'price')
            ->where('del_flg', null)
            ->orderBy('product_id', 'asc')
            ->paginate(12);

        foreach ($items as $item) {
            $item->pic = explode(',', $item->pic)[0];
            $item->price = "￥" . number_format($item->price);
        }
        return $items;
    }

    //商品詳細
    public function getProductDetailList($id)
    {
        try{
            $items = $this->with('productDetailModel')
                ->where('product_id', $id)
                ->first();
            $pics = explode(',', $items->pic);
            array_pop($pics);
            $data = [
                'items' => $items,
                'pics' => $pics,
            ];
            // var_dump($data['items']['product_name']);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $data;
    }

    //カート内商品取得(未ログインユーザー)
    public function cartItems($productId){
        // $items = [];
        if(!empty($productId)){
                $items = $this->db->table('m_products')
                    ->where('product_id', $productId)
                    ->first();
                // $products[] = [
                //     'product_id' => $item->product_id,
                //     'del' => $item->del_flg,
                //     'product_name' => $item->product_name,
                //     'pic' => explode(',', $item->pic)[0],
                //     'color' => $item->color,
                //     'unit_price' => $item->unit_price,
                //     'tax' => $item->tax,
                //     'price' => $item->price,
                //     'status' => $item->status,
                // ];
        }  
        return $items;
    }

    // カートモデルの呼び出し
    // public function cartModel()
    // {
    //     return $this->hasMany('App\Model\CartModel', 'product_id', 'product_id');
    // }
}
