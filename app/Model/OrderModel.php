<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderModel extends Model
{
    protected $table = 't_orders';
    protected $db;

    public function __construct()
    {
        $this->db = DB::connection();
    }

    //削除（論理削除処理）
    public function getOrderDelete($orderid)
    {
        try {
            $ret = $this->db->table('t_orders')
            ->where('order_id', $orderid)
                ->update([
                    'del_flg'              => 1,
                ]);
        } catch (Exception $e) {
            var_dump($e);
        }
        return;
    }

    // 商品モデルの呼び出し
    public function productModel()
    {
        return $this->belongsTo('App\Model\ProductModel', 'product_id', 'product_id');
    }

    // 商品モデルの呼び出し
    public function orderStatementModel()
    {
        return $this->belongsTo('App\Model\OrderStatementModel', 'statement_id', 'statement_id');
    }

    // 価格の表示
    public function showPrice($key)
    {
        return "￥". number_format($this->{$key});
    }
}
