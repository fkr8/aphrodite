<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProfitModel extends Model
{
    protected $table = 'm_profits';
    protected $db;

    public function __construct()
    {
        $this->db = DB::connection();
    }

    //削除（論理削除処理）
    public function getOrderDelete($orderid)
    {
        try {
            $ret = $this->db->table($this->table)
                ->where('order_id', $orderid)
                ->update([
                    'del_flg'              => 1,
                ]);
        } catch (Exception $e) {
            var_dump($e);
        }
        return;
    }

    // 注文明細モデルの呼び出し
    public function orderStatementModel()
    {
        return $this->belongsTo('App\Model\OrderStatementModel', 'statement_id', 'statement_id');
    }
}
