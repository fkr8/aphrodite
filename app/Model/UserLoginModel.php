<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserLoginModel extends Model
{
    //
    protected $table;
    protected $db;

    public function __construct()
    {
        $this->table = 'm_users';
        $this->db = DB::connection();
    }

    //ログイン処理
    public function userLogin($request)
    {

        $email = $request->email;
        $password = $request->password;

        $this->db->beginTransaction();
        try {
                $log_data = $this->db->table($this->table)
                        ->select('user_id', 'name', 'authority','email', 'password')
                        ->where('email', $email)
                        ->first();
                if ($log_data != null) {
                    $hashed_pass = $log_data->password;
                } else {
                    return null;
                }
                
        $this->db->commit();
        } catch (Exception $e) {
            //不明な失敗
            $this->db->rollback();
            echo '<script type="text/javascript">';
            echo 'alert("エラーが発生しました。処理は失敗しました。");';
            echo 'location.href="/login"';
            echo '</script>';
            exit;
        }      

        if (\Hash::check($password,$hashed_pass)) {
            //ここの書き方（id,name,authority)
            $login  = [
                'user_id' => $log_data->user_id,
                'name' => $log_data->name,
                'authority' => $log_data->authority
            ]; 

            return $login;
        } else {
            return null;
        }

        // $this->db->beginTransaction();
        // try {

        //     $login = $this->db->table($this->table)
        //         ->select('user_id', 'name', 'authority')
        //         ->where([
        //             ['email', $email],
        //             ['password', $password]
        //         ])
        //         ->first();
        //     // 成功したらセッション保存
        //     return $login; //sesData
            
        // $this->db->commit();
        // } catch (Exception $e) {
        //     //不明な失敗
        //     $this->db->rollback();
        //     echo '<script type="text/javascript">';
        //     echo 'alert("エラーが発生しました。処理は失敗しました。");';
        //     echo 'location.href="/login"';
        //     echo '</script>';
        //     exit;
        // }
    }
}
