<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserModel extends Model
{
    //
    protected $table;
    protected $db;

    public function __construct()
    {
        $this->table = 'm_users';
        $this->db = DB::connection();
    }

    //ユーザテーブル表示
    public function userIndex($userId)
    {
        try {
            $items = $this->db->table($this->table)
                ->where('del_flg', null)
                ->orderBy('user_id', 'asc')
                ->paginate(10);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $items;
    }

    //名前検索
    public function userSearch($request)
    {
        try {
            $user_search = $request->user_search;
            $items = DB::table('m_users')
                ->where([
                    ['del_flg', null],
                    ['name', 'like', '%' . $user_search . '%']
                ])
                ->orderBy('user_id', 'asc')
                ->paginate(10);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $items;
    }

    //削除（論理削除処理）
    public function getUserDelete($userid)
    {
        try {
            $this->db->table('m_users')
                ->where('user_id', $userid)
                ->update([
                    'del_flg'              => 1,
                ]);
        } catch (Exception $e) {
            var_dump($e);
        }
        return;
    }

    //編集画面の出力
    public function getUserEdit($request)
    {
        try {
            $userId = $request->input('code');
            $items = DB::table('m_users')
                ->where('user_id', $userId)
                ->first();
        } catch (Exception $e) {
            var_dump($e);
        }
        return $items;
    }

    //編集登録
    public function getUserUpdate($param)
    {
        try {
            $this->db->table('m_users')
                ->where('user_id', $param->user_id)
                ->update([
                    'user_id' => $param->user_id,
                    'name' => $param->name,
                    'tel' => $param->tel,
                    'email' => $param->email,
                    'age' => $param->age,
                ]);
        } catch (Exception $e) {
            var_dump($e);
        }
        return;
    }
}

//   //削除（物理削除処理）
//   public function getUserDelete($userid){
//     try {
//     $ret = $this->db->table('m_users')
//     ->where('user_id', $userid)
//     ->delete();
//     } catch (Exception $e) {
//       var_dump($e);
//     }
//     return;
//     }
// }
