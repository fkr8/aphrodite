<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ProductModel;
use App\Http\Requests\ProductsRequest;
use App\Model\ProductDetailModel;

class ProductsController extends Controller
{
    //商品一覧呼び出し
    public function index(Request $request)
    {
        $Product = new ProductModel();
        $items = $Product->productIndex($request);
        return view('products.product', ['items' => $items]);
    }

    //名前検索
    public function productPost(Request $request)
    {
        $Product = new ProductModel();
        $items = $Product->productSearch($request);
        return view('products.product', ['items' => $items]);
    }

    //商品編集
    public function productEdit($id)
    {
        $Product = new ProductModel();
        $data = $Product->getProductEdit($id);
        return view('products.productEdit', $data);
    }

    //編集登録
    public function productUpdate(ProductsRequest $request)
    {
        $Product = new ProductModel();
        $Product->productUpdate($request);
        $ProductDetail = new ProductDetailModel();
        $ProductDetail->productDetailUpdate($request);
        return redirect('/product');
    }

    //商品削除
    public function productDelete(Request $request)
    {
        $Product = new ProductModel();
        $checkList = $request->check;
        $Product->productDelete($checkList);
        return redirect('/product');
    }

    //新規登録
    public function productRegist(ProductsRequest $request)
    {
        $Product = new ProductModel();
        $Product->productRegist($request);
        return redirect('/product');
    }

        // echo '<script>';
        // echo 'console.log('. json_encode( mb_get_info() ) .');';
        // echo 'console.log('. json_encode( mb_convert_encoding($_POST["content"], "ASCII", "UTF-8")) .');';
        // echo '</script>';
        // return view('welcome');
}
