<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\ProfitModel;

class HomeData
{
    public $monthSales;             // 今月の売上
    public $todaySales;             // 今日の売上
    public $yesterdaySales;         // 昨日の売上
    public $weeklyGraph;            // 週間グラフ
    public $yearlyGraph;            // 年間グラフ

    private static $homeData;
    public static function getInstance()
    {
        if (!isset(self::$homeData)) {
            self::$homeData = new HomeData();
        }
        self::$homeData->getMonthSales();
        self::$homeData->getTodaySales();
        self::$homeData->getYesterdaySales();
        self::$homeData->getWeeklyGraph();
        self::$homeData->getYearlyGraph();

        return self::$homeData;
    }

    // 今月の売上を取得
    private function getMonthSales()
    {
        $from = $this->changeMonth(0);
        $to = $this->changeDate(strtotime($this->changeMonth(-1)), 1);
        $sales = $this->getSalesForSpecifiedPeriod($from, $to);
        $this->monthSales = $this->showSalesCondition($sales['amount'], $sales['volume']);
    }

    // 今日の売上を取得
    private function getTodaySales()
    {
        $from = date('Y-m-d', time());
        $to = $this->changeDate(time(), -1);
        $sales = $this->getSalesForSpecifiedPeriod($from, $to);
        $this->todaySales = $this->showSalesCondition($sales['amount'], $sales['volume']);
    }

    // 昨日の売上を取得
    private function getYesterdaySales()
    {
        $from = $this->changeDate(time(), 1);
        $to = date('Y-m-d', time());
        $sales = $this->getSalesForSpecifiedPeriod($from, $to);
        $this->yesterdaySales = $this->showSalesCondition($sales['amount'], $sales['volume']);
    }

    // 週間グラフ取得
    private function getWeeklyGraph()
    {
        for ($i = 0; $i < 7; $i++) {
            $from = $this->changeDate(time(), 7 - $i);
            $to = $this->changeDate(time(), 6 - $i);

            // 日の売上状況を取得
            $date[$i] = substr($from, 5);
            $sales[$i] = $this->getSalesForSpecifiedPeriod($from, $to)['amount'];
        }
        $this->weeklyGraph = ['date' => $date, 'sales' => $sales];
    }

    // 年間グラフ取得
    private function getYearlyGraph()
    {
        for ($i = 0; $i < 12; $i++) {
            $from = $this->changeMonth(12 - $i);                                // 月頭
            $to = $this->changeDate(strtotime($this->changeMonth(11 - $i)), 1);     // 月末

            // 月の売上状況を取得
            $date[$i] = substr($from, 0, -3);
            $sales[$i] = $this->getSalesForSpecifiedPeriod($from, $to)['amount'];
        }
        $this->yearlyGraph = ['date' => $date, 'sales' => $sales];
    }

    // 指定の期間の売上数と件数の取得
    private function getSalesForSpecifiedPeriod($from, $to)
    {
        $salesAmount = 0;
        $salesVolume = 0;

        $tableItems = ProfitModel::where('del_flg', null)->get('statement_id');

        foreach ($tableItems as $item) {
            if (isset($item)) {
                $order_date = strtotime($item->orderStatementModel->order_date);
                if (($order_date >= strtotime($from)) && ($order_date < strtotime($to))) {
                    $salesAmount += $item->orderStatementModel->all_price;
                    $salesVolume++;
                }
            }
        }
        return ['amount' => $salesAmount, 'volume' => $salesVolume];
    }

    // 日付を指定の月数戻す
    private function changeMonth($offsetMonth)
    {
        return date('Y-m-01', strtotime(date('Y-m-01') . '-' . $offsetMonth . ' month'));
    }

    // 日付を指定の日数戻す
    private function changeDate($date, $offsetDay)
    {
        return date('Y-m-d', $date - $offsetDay * 60 * 60 * 24);
    }

    // 日時から時間を取り除く
    private function removeTime($date)
    {
        return date('Y-m-d', floor(strtotime($date)));
    }

    // 売上状況の表示
    private function showSalesCondition($salesAmount, $salesVolume)
    {
        return "￥" . number_format($salesAmount) . " / " . $salesVolume . "件";
    }
}