<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ProductModel;

class UserHomeController extends Controller
{
    //商品一覧呼び出し
    public function index(Request $request)
    {
        $Product = new ProductModel();
        $items = $Product->getProductList($request);
        return view('userHome.userHome', ['items' => $items]);
    }

    //商品詳細一覧呼び出し
    public function indexDetail($id)
    {
        $ProductDetail = new ProductModel();
        $items = $ProductDetail->getProductDetailList($id);
        return view('products.productDetail', ['items' => $items]);
    }
}