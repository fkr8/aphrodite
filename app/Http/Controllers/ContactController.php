<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Model\UserModel;

class ContactController extends Controller
{
    //お問い合わせ確認
    public function contactComfirm(ContactRequest $request)
    {
        $subject = ['商品について', 'ライブブッキング', 'その他'];
        $contactData = [
            'name' => $request->name,
            'mail' => $request->mail,
            'subject' => $subject[$request->subject],
            'content' => $request->content,
        ];
        $request->session()->put('contact', $contactData);
        return view('contact.contactComfirm', $contactData);
    }

    //お問い合わせ送信
    public function contactSend(Request $request)
    {
        //お問い合わせ内容
        $contactData = $request->session()->get('contact');
        //管理者送信
        $titleAdmin = 'お問い合わせメール';
        $viewAdmin = 'mail.contactMailAdmin';
        $MailAdmin = \App\Model\UserModel::where('authority', 1)->first('email');
        Mail::to($MailAdmin)->send(new SendMail($titleAdmin, $viewAdmin, $contactData));
        //ユーザー送信
        $title = 'お問い合わせ確認メール';
        $view = 'mail.contactMail';
        Mail::to($contactData['mail'])->send(new SendMail($title, $view, $contactData));
        //セッション削除
        $request->session()->forget('contact');
        return redirect('/complete');
    }
}
