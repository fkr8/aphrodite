<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsersLogoutController extends Controller
{
        //ログアウト
        public function postLogout(Request $request)
        {
            $request->session()->flush();
            //全画面作成したらhome画面に遷移
            return redirect('/user_home');
        }
}
