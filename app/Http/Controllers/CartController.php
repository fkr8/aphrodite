<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\CartModel;
use App\Model\ProductModel;
use App\Model\ProductDetailModel;

class CartController extends Controller
{
    //カート確認  R4.1.20作成中
    public function cartConfirm(Request $request)
    {
        $user = $request->session()->get('ses_data');
        if(empty($user)){ //セッションから取得(idのみ)
            $cartData = $request->session()->get('cart_data');
            $Product = new ProductModel();
            $items = $Product->cartItems($cartData);

            // 非公開or削除商品のセッション削除
            foreach ($items as $item) {
                if($item['del']==1 || $item['status']==0){
                    unset($cartData[$item['product_id']]);
                }
            }
            $request->session()->put('cart_data', $cartData);

        } else { //DBから取得
            $userId = $user['user_id'];
            $Cart = new CartModel();
            $items = $Cart->cartItems($userId);
        }
        return view('cart.cart', ['items' => $items,]);
    }

    //カート商品追加
    public function cartAdd(Request $request)
    {
        $user = $request->session()->get('ses_data');
        $productId = $request->productId;//detail_idじゃないとダメだから、不要？9/28
        $quantity = $request->quantity;
        //商品情報の取得
        $Product = new ProductModel();
        $products = $Product->cartItems($productId);
        //詳細情報の取得
        $Detail = new ProductDetailModel();
        $detailId = $Detail->cartDetailId($request);
        $size = $request->size;
        //選択した商品詳細の情報 
        $items[] = [
            'product_id' => $products->product_id,
            'del' => $products->del_flg,
            'product_name' => $products->product_name,
            'pic' => explode(',', $products->pic)[0],
            'color' => $products->color,
            'unit_price' => $products->unit_price,
            'tax' => $products->tax,
            'price' => $products->price,
            'status' => $products->status,
            //detail
            'product_detail_id' => $detailId,
            'size' => $size,
            'quantity' => $quantity,
        ];
        //ログインしてなければセッション出力、してればカートデータを保存し出力。
        if(empty($user)) { //session pushで配列に保存
            $request->session()->push('cart_data', $items);
            $request->session()->get('cart_data');
            // if(isset($cartData[$detailId])){
            //     $cartData[$detailId] += $quantity;
            // } else {
            //     $cartData[$detailId] = $quantity;
            // }
            var_dump($items);
            return view('cart.cart',['items' => $items,]);
        } else if(!empty($user)) { //DBに保存
            $userId = $user['user_id'];
            $Cart = new CartModel();
            $cartItems = $Cart->cartAdd($userId,$items);
            return view('cart.cart',['items' => $cartItems,]);
        }

    }


    //カート内商品削除
    public function cartDelete(Request $request)
    {
        $user = $request->session()->get('ses_data');
        $detailId = $request->detailId;

        if(empty($user)){ //セッションから削除
            $cartData = $request->session()->get('cart_data');
            unset($cartData[$detailId]);
            $request->session()->put('cart_data', $cartData);
        } else { //DBから削除
            $userId = $user['user_id'];
            $Cart = new CartModel();
            $Cart->cartDelete($userId, $detailId);
        }
        return response()->json();
    }

    //カート内数量変更
    public function cartChangeQuantity(Request $request)
    {
        $user = $request->session()->get('ses_data');
        $detailId = $request->detailId;
        $quantity = $request->quantity;

        if(empty($user)){ //セッションの数量変化
            $cartData = $request->session()->get('cart_data');
            $cartData[$detailId] = $quantity;
            $request->session()->put('cart_data', $cartData);
        } else { //DBの数量変化
            $userId = $user['user_id'];
            $Cart = new CartModel();
            $Cart->cartChangeQuantity($userId, $detailId, $quantity);
        }
        return response()->json();
    }

    //カート内購入確認
    public function cartPurchase(Request $request)
    {
        $user = $request->session()->get('ses_data');
        $userId = $user['user_id'];
        $Cart = new CartModel();
        $items = $Cart->cartPurchase($userId);
        \Debugbar::info($items);
        return view('cart.cartPurchase', ['items' => $items]);
    }

    //カート内購入処理
    public function purchaseComplete(Request $request)
    {
        $user = $request->session()->get('ses_data');
        $userId = $user['user_id'];;
        $Cart = new CartModel();
        $Cart->purchaseComplete($userId);
        return redirect('/complete');
    }
}