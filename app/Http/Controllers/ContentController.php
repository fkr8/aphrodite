<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\ContentModel;

class ContentController extends Controller
{
    //ABOUTテーブル表示
    public function index(Request $request)
    {
        $Content   = new ContentModel();
        $items = $Content->contentIndex($request);
        // $items = DB::table('m_about')->where('del_flg', null)->orderBy('about_id','asc')->paginate(10);
        return view('content.content', ['items' => $items]);
    }

    //ABOUT新規登録
    public function add(Request $request)
    {
        return view('contentResist');
    }

    //ABOUT新規登録
    public function create(Request $request)
    {
        if ('about' !== '') {
            $Content   = new ContentModel();
            $items = $Content->contentCreate($request);
            return redirect('/content');
        } else {
        }
    }

    //編集画面呼び出し
    public function contentEdit(Request $request)
    {
        $Content = new ContentModel();
        $items = $Content->getContentEdit($request);
        return view('content.contentEdit', ['items' => $items]);
    }

    //編集登録
    public function update(Request $request)
    {
        $db = DB::connection();
        $db->beginTransaction();
        try {

            $Content   = new ContentModel();
            $Content->getContentUpdate($request);

            $db->commit();
        } catch (Exception $e) {
            $db->rollback();
            echo '<script type="text/javascript">';
            echo 'alert("エラーが発生しました。編集処理は失敗しました。");';
            echo 'location.href="/"';
            echo '</script>';
            exit;
        }
        return redirect('/content');
    }

    //削除
    public function contentDelete(Request $request)
    {

        //Model呼び出し
        $Content   = new ContentModel();
        $checkbox = $request->check;
        //一つもチェックがない場合エラー
        $db = DB::connection();
        $db->beginTransaction();
        try {
            if (!empty($checkbox)) {
                foreach ($checkbox as $aboutid) {
                    //ユーザ情報を削除

                    $Content->getContentDelete($aboutid);
                }
                $db->commit();
            } else {
                echo '<script type="text/javascript">';
                echo 'alert("チェックボックスを選択してから削除してください");';
                echo 'location.href="/content"';
                echo '</script>';
                exit;
            }
        } catch (\Exception $e) {
            $db->rollback();
            echo '<script type="text/javascript">';
            echo 'alert("エラーが発生しました。選択行削除処理は失敗しました。");';
            echo 'location.href="/content"';
            echo '</script>';
            exit;
        }

        return redirect('/content');
    }
}
