<?php

namespace App\Http\Controllers;

use App\Model\UserRegistModel;
use App\Http\Requests\RegistRequest;

class RegistsController extends Controller
{
    public function getRegist()
    {
        return view('regists.regist');
    }

    public function postRegist(RegistRequest $request)
    {

        $Regist = new UserRegistModel();
        $Regist->userRegist($request);

        return view ('test');

    }
}