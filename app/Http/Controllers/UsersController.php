<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\UserModel;
use App\http\Requests\UsersRequest; //バリデーション(編集中)

class UsersController extends Controller
{
    //ユーザテーブル表示
    public function index(Request $request)
    {
        //Model呼び出し
        $User   = new UserModel();
        $items = $User->userIndex($request);
        return view('users.index', ['items' => $items]);
    }

    //名前検索
    public function userPost(Request $request)
    {
        $User   = new UserModel();
        $items = $User->userSearch($request);
        return view('users.index', ['items' => $items]);
    }

    //削除
    public function userDelete(Request $request)
    {
        $User   = new UserModel();
        $checkbox = $request->check;

        //一つもチェックがない場合エラー
        $db = DB::connection();
        $db->beginTransaction();
        try {
            if (!empty($checkbox)) {
                foreach ($checkbox as $userid) {
                    //ユーザ情報を削除

                    $User->getUserDelete($userid);
                }
                $db->commit();
            } else {
                echo '<script type="text/javascript">';
                echo 'alert("チェックボックスを選択してから削除してください");';
                echo 'location.href="/"';
                echo '</script>';
                exit;
            }
        } catch (\Exception $e) {
            $db->rollback();
            echo '<script type="text/javascript">';
            echo 'alert("エラーが発生しました。選択行削除処理は失敗しました。");';
            echo 'location.href="/"';
            echo '</script>';
            exit;
        }

        return redirect('/');
    }


    //編集画面呼び出し
    public function userEdit(Request $request)
    {
        $User = new UserModel();
        $items = $User->getUserEdit($request);
        return view('users.userEdit', ['items' => $items]);
    }

    //バリデーション
    // public function validate(UsersRequest $request)
    // {

    // }

    //修正登録

    public function update(Request $request)
    {
        // echo '<pre>';
        // var_dump($request);
        // echo '</pre>';
        // exit;

        $db = DB::connection();
        $db->beginTransaction();
        try {

            $User   = new UserModel();
            $User->getUserUpdate($request);

            $db->commit();
        } catch (Exception $e) {
            $db->rollback();
            echo '<script type="text/javascript">';
            echo 'alert("エラーが発生しました。編集処理は失敗しました。");';
            echo 'location.href="/"';
            echo '</script>';
            exit;
        }
        return redirect('/');
    }
}
