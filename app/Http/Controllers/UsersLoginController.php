<?php

namespace App\Http\Controllers;

use App\Model\UserLoginModel;
use App\Model\CartModel;
use App\Http\Requests\LoginRequest;

class UsersLoginController extends Controller
{
    //画面表示
    public function getLogin()
    {
        return view('login.login');
    }

    //バリデーションと認証
    public function postLogin(LoginRequest $request)
    {

        $Login   = new UserLoginModel();
        $items = $Login->userLogin($request);

        if ($items != null) {
            $request->session()->put('ses_data', $items);
            // $ses_data = $items->name;
            $cartData = $request->session()->get('cart_data');
            //セッションにカート情報があればDBに追加しセッションは削除
            if(!empty($cartData)){
                $userId = $items['user_id'];
                $Cart = new CartModel();
                foreach ($cartData as $productId => $quantity) {
                    $Cart->cartAdd($userId, $productId, $quantity);
                }
                $request->session()->forget('cart_data');
            }
            return redirect('/user_home');
        } else {
            return view('login.login', ['msg' => '会員情報がありません。']);
        }
    }

}
