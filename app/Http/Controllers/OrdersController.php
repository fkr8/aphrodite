<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\OrderModel;

class OrdersController extends Controller
{
    //注文テーブル表示
    public function index(Request $request)
    {
        //Model呼び出し
        $items = OrderModel::where('del_flg', null)->orderBy('order_id', 'asc')->paginate(10);
        return view('orders.orders', ['items' => $items]);
    }

    //削除
    public function orderDelete(Request $request)
    {
        $Order   = new OrderModel();
        $checkbox = $request->check;

        //一つもチェックがない場合エラー
        $db = DB::connection();
        $db->beginTransaction();
        try {
            if (!empty($checkbox)) {
                foreach ($checkbox as $orderid) {
                    //ユーザ情報を削除

                    $Order->getOrderDelete($orderid);
                }
                $db->commit();
            } else {
                echo '<script type="text/javascript">';
                echo 'alert("チェックボックスを選択してから削除してください");';
                echo 'location.href="/order"';
                echo '</script>';
                exit;
            }
        } catch (\Exception $e) {
            $db->rollback();
            echo '<script type="text/javascript">';
            echo 'alert("エラーが発生しました。選択行削除処理は失敗しました。");';
            echo 'location.href="/order"';
            echo '</script>';
            exit;
        }
        return redirect('/order');
    }
}
