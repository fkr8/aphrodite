<?php

namespace App\Http\Controllers;

use App\Model\NewsModel;
use Illuminate\Http\Request;

class UsersNewsController extends Controller
{
    public function getNews(Request $request){
    $News = new NewsModel();
    $items = $News->getAbout($request);
    \Debugbar::info($items);
    return view('news.news',['items' => $items]);
    }
}