<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\ProfitModel;
use App\Http\Controllers\HomeData;

class HomeController extends Controller
{
    //売上テーブル表示
    public function index(Request $request)
    {
        $dispGraph = $request->graph;
        if ($dispGraph == "") {
            $dispGraph = "weekly";
        }
        $items = $this->getHomeData($dispGraph);
        return view('home.home', ['items' => $items]);
    }

    public function getHomeData($dispGraph)
    {
        $homeData = HomeData::getInstance();

        $monthSales = $homeData->monthSales;               // 今月の売上
        $todaySales = $homeData->todaySales;               // 今日の売上
        $yesterdaySales = $homeData->yesterdaySales;       // 昨日の売上
        // 売上状況（今月、今日、昨日）
        $salesCondition = ['monthSales' => $monthSales, 'todaySales' => $todaySales, 'yesterdaySales' => $yesterdaySales];

        if ($dispGraph == "weekly") {
            $salesGraph = $homeData->weeklyGraph;             // 週間グラフ取得
        } else {
            $salesGraph = $homeData->yearlyGraph;             // 年間グラフ取得
        }
        return ['salesCondition' => $salesCondition, 'salesGraph' => $salesGraph, 'dispGraph' => $dispGraph];
    }
}
