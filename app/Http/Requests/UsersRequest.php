<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'user_id',
            'edit_name' => 'required',
            'edit_tel' => 'numeric',
            'edit_email' => 'email,',
            'check_email' => 'email',
            'edit_age' => 'numelic|between:0,100',
         ];
    }

    public function messages()
    {
        return [
            'edit_name.required' => '名前を入力してください',
            'password.required' => 'パスワードを入力してください',
        ];
    }
}
