<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if($this->path() == "contact_comfirm"){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'mail' => 'required|email',
            'content' => 'required',
        ];
    }

    // メッセージ
    public function messages()
    {
        return [
            'name.required' => 'お名前を入力してください。',
            'mail.required' => 'メールアドレスを入力してください。',
            'mail.email' => 'メールアドレスを入力してください。',
            'content.required' => 'お問い合わせ内容を入力してください。',
        ];
    }
}
