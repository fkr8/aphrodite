<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class ProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if($this->path() == "product_regist" || $this->path() == "product_update"){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id',
            'product_name'=>'required',
            'pic_remine',
            'content'=>'required',
            'unit_price'=>'required|numeric',
            'status',
            //詳細
            'product_detail_id',
            //詳細：配列でわたるのでvaliエラー。
            'size',
            'color',
            'stock',
        ];
        if ('status'==0) {
            return [            
                //release_date           
                'date'=>'required|after_or_equal:today',
                'time'=>'required',
            ];
        }
    }

    // メッセージ
    public function messages()
    {
        return [
            'product_name.required' => '商品名を入力してください。',
            'content.required' => '商品説明を入力してください。',
            'unit_price.required' => '商品価格を入力してください。',
            'unit_price.numeric' => '商品価格は半角数字を入力してください。',
            'date.required' => '公開日を入力してください。',
            'date.after_or_equal' => '現在以降の日付を入力してください。',
            'time.required' => '公開時間を入力してください。',
            'time.after_or_equal' => '現在以降の時間を入力してください。',
        ];
    }

    // 日付時間の条件
    public function withValidator(Validator $vali)
    {
        // 非公開ならdateは必須かつ今日以降
        $vali->sometimes('date', 'required|after_or_equal:today', function($input){
            return $input->status==0;
        });
        // 非公開ならtimeは必須
        $vali->sometimes('time', 'required', function($input){
            return $input->status==0;
        });
        // 非公開かつdateが今日ならtimeは今以降
        $vali->sometimes('time', 'after_or_equal:now', function($input){
            $today = date("Y-m-d");
            return $input->status==0 && $input->date==$today;
        });
    }
}
