<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'zip' => 'required|numeric|digits:7',
            'address' => 'required',
            'tel' => 'required|numeric',
            'age' =>'required|numeric|between:18,90',
            'email' => 'bail|email|unique:m_users,email|confirmed',
            'password' => 'required|regex:/^(?=.*?[a-z])(?=.*?\d)[a-z\d]{4,15}$/i|confirmed',
            
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '名前は必ず入力してください。',
            'zip.required' => '郵便番号が入力されていません。',
            'zip.numeric' => '郵便番号は半角数字で入力してください。',
            'zip.digits' => '郵便番号は７桁で入力してください。',
            'address.required' => '住所を入力してください。',
            'tel.required' => '電話番号が入力されていません。',
            'tel.numeric' => '電話番号は半角数字で入力してください。',
            'age.required' => '年齢が入力されていません。',
            'age.numeric' => '年齢は半角数字で入力してください。',
            'age.between' => '年齢は18歳以上で入力してください。',
            'email.email' => 'メールアドレスが無効です。',
            'email.unique' => '既に登録されているメールアドレスです。',
            'email.confirmed' => 'メールアドレスが一致しません。',
            'password.required' => 'パスワードが入力されていません。',
            'password.regex' => 'パスワードは英数字を混ぜ４～１５文字で入力してください。',
            'password.confirmed' => 'パスワードが一致しません。'
            
        ];
    }


}
